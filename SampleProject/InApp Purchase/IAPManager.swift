//
//  IAPManager.swift
//  prefGrab
//
//  Created by RAVI K GUPTA on 23/11/21.
//

import StoreKit
///*

protocol IAPManagerDelegate: AnyObject {
    func fetched(products: [IAProduct])
    func itemPurchased(params: [String: Any])//, detail: SubscriptionStatusData)
}
 
enum IAPManagerProductStatus: String{
    case disabled = "Purchases are disabled in your device!"
    case restored = "You've successfully restored your purchase!"
    case purchased = "You've successfully bought this item!"
    case noProducts = "No products are available"
    case purchasing = "Purchasing is in progress"
    case defered = "Purchasing is in progress and requires action"

}
struct IAProduct {
    let description: String
    let title: String
    let price : NSDecimalNumber
    let identifier: String
    let priceSymbol: String
    let priceWithCurrency: String
}

class IAPManager: NSObject {
    static let shared = IAPManager()
    fileprivate var productsRequest = SKProductsRequest()
    public var iapProducts = [SKProduct]()
    var showLoader = false
    var currentPurchaseStatus: IAPManagerProductStatus?
    weak var delegate: IAPManagerDelegate?
    var shouldRequestServer = true
    var currentPurchaseIdentifier: String?
    //static let monthlySubscriptionIdentifier = "com.prefgrab.monthly"
    //static let yearlySubscriptionIdentifier = "com.prefgrab.yearly"

    func canMakePurchases() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
 
    private override init()
    {
        super.init()
        SKPaymentQueue.default().add(self)
//        if #available(iOS 13.0, *) {
//            SKPaymentQueue.default().storefront
//        } else {
//            // Fallback on earlier versions
//        }
        //DisplayBanner.show(message: "private override init")
    }
    
    func purchaseMyProduct(identifier: String){
        if iapProducts.count == 0 {
            currentPurchaseStatus = .noProducts
            //MARK:
            
            kAppDelegate.getSubscriptionList()
            return
            
        }
        
        if self.canMakePurchases() {
            currentPurchaseIdentifier = identifier
            guard let product = iapProducts.first(where: {$0.productIdentifier == identifier}) else{
                DisplayBanner.failureBanner(message: "Invalid Purchase \(identifier)")
                return
            }
            Loader.show()
            shouldRequestServer = true
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
        } else {
            currentPurchaseStatus = .noProducts
            DisplayBanner.infoBanner(message: "You can not make purchase.")
        }
    }
    
    func restorePurchase(){
        Loader.show()
        shouldRequestServer = true
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func fetchAvailableProducts(showLoader: Bool){
        self.showLoader = showLoader
        /*if iapProducts.count > 0{
            return
         }
         */
        if showLoader {
            Loader.show()
        }
        //productsRequest = SKProductsRequest(productIdentifiers: Set(arrayLiteral: IAPManager.monthlySubscriptionIdentifier,IAPManager.yearlySubscriptionIdentifier))
        let filteredArray = kAppDelegate.subscriptionListObj?.data?.filter({$0.subscriptionID != ""})
        let productArray = filteredArray?.map({$0.subscriptionID ?? ""})
        guard let array = productArray else {
            print("No Products avl")
            return
        }
        Console.log("Request For Products")
        productsRequest = SKProductsRequest(productIdentifiers: Set(array))
        productsRequest.delegate = self
        productsRequest.start()
    }
    func getProducts() -> [IAProduct]{
        var products = [IAProduct]()
        for product in iapProducts{
            let numberFormatter = NumberFormatter()
            numberFormatter.formatterBehavior = .behavior10_4
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = product.priceLocale
            let price1Str = numberFormatter.string(from: product.price)
            print(product.localizedDescription + "\nfor just \(price1Str!)")
            products.append(IAProduct(description: product.description, title: product.localizedTitle, price: product.price, identifier: product.productIdentifier, priceSymbol: numberFormatter.currencySymbol ?? "error", priceWithCurrency:  price1Str ?? "error"))
        }
        return products
    }
    
    func removeObserver(){
        SKPaymentQueue.default().remove(self)
    }
}

extension IAPManager: SKProductsRequestDelegate{
    func request(_ request: SKRequest, didFailWithError error: Error) {
        if showLoader {
             Loader.hide()
        }
        Console.log("didFailWithError \(error.localizedDescription)")
       // DisplayBanner.show(message: error.localizedDescription)
    }
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        print(response.invalidProductIdentifiers)
        if showLoader {
            Loader.hide()
        }
        if (response.products.count > 0) {
            iapProducts = response.products
            var products = [IAProduct]()
            for product in iapProducts{
                let numberFormatter = NumberFormatter()
                //numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .currency
                numberFormatter.locale = product.priceLocale
                let price1Str = numberFormatter.string(from: product.price)
                print(product.localizedDescription + "\nfor just \(price1Str!)")
                products.append(IAProduct(description: product.description, title: product.localizedTitle, price: product.price, identifier: product.productIdentifier, priceSymbol: price1Str ?? "error", priceWithCurrency: price1Str ?? "error"))
            }
            delegate?.fetched(products: products)
        }else {
            currentPurchaseStatus = .noProducts
            Console.log("No product available.")
        }
    }
}
extension IAPManager: SKPaymentTransactionObserver{
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                print("purchased")
                Loader.hide()
                SKPaymentQueue.default().finishTransaction(transaction)
                itemPurchasedSuccessfully(identifier: transaction.payment.productIdentifier)
            case .failed:
                print("failed")
                Loader.hide()
               // (transaction.error as? SKError)?.code == SKError.paymentCancelled
                SKPaymentQueue.default().finishTransaction(transaction)
            case .restored:
                print("restored")
                Loader.hide()
                printReceiptData()
                itemPurchasedSuccessfully(identifier: transaction.payment.productIdentifier)
                //itemPurchasedSuccessfully(transactionIdent:transaction.transactionIdentifier)
                SKPaymentQueue.default().finishTransaction(transaction)
            case .purchasing :
                print("purchasing")
            case .deferred :
                print("deferred")
            @unknown default:
                print("unknown")
                Loader.hide()
            }
            if let _ = transaction.error{
                //DisplayBanner.failureBanner(message: error.localizedDescription)
            }
        }
    }
    
    func printReceiptData() {
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        if let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) {
            print("printReceiptData\(recieptString)\n")
        }
    }
    
    private func itemPurchasedSuccessfully(identifier:String){
        if !shouldRequestServer {
            Console.log("itemPurchasedSuccessfully attampted to request server")
            return
        }
        shouldRequestServer = false
        guard let product = getProducts().first(where: {$0.identifier == identifier}) else{
           // DisplayBanner.show(title: "Invalid identifier.")
            return
        }

        Console.log("itemPurchasedSuccessfully \(identifier)")
        var params = [String: Any]()
        /*
        if identifier == IAPManager.monthlySubscriptionIdentifier{
            params["duration"] = "1 MONTH"
            params["subscriptionType"] = "MONTHLY"
        }else{
            params["duration"] = "1 YEAR"
            params["subscriptionType"] = "YEARLY"
        }
        */
        params["currencySymbol"] = product.priceSymbol
        params["price"] = "\(product.price)"
        params["device_type"] = "IOS"
        //params["password"] = "0e7ea95edada4ae494352d50188564ba"
        params["subscription_id"] = identifier
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                print(receiptData)
                
                let receiptString = receiptData.base64EncodedString(options: [])
                params["purchase_token"] = receiptString
                delegate?.itemPurchased(params: params)//, detail: SubscriptionStatusData(isSubscribed: true, identifier: identifier))
                
                print("Great Product Purchased and params are :- \(params)")
            }
            catch {
                print("Couldn't read receipt data with error: " + error.localizedDescription)
                Console.log("Couldn't read receipt data with error: " + error.localizedDescription) }
        }
        
    }
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        Loader.hide()
        if queue.transactions.count > 0 {
            DisplayBanner.successBanner(message: "Purchase restored successfully")
        }
        else {
            DisplayBanner.infoBanner(message: "There is no purchase to restore")
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        Loader.hide()
        DisplayBanner.failureBanner(message: error.localizedDescription)
    }
  }


//*/
