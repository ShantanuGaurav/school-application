//
//  StartResultVC.swift
//  SampleProject
//
//  Created by Shivam Garg on 04/03/22.
//

import UIKit

class StartResultVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var viewSinglePicture: UIView!
    
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var stckVwFrstRow: UIStackView!
    @IBOutlet weak var stckVwSecondRow: UIStackView!
    
    
    @IBOutlet weak var imgViewSinglePic: UIImageView!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView3: UIImageView!
    @IBOutlet weak var imgView4: UIImageView!
    
    var imagesArray = [SelectedImageStruct]()
    var remainingImagesArray = [SelectedImageStruct]()
    var selectedBtnCount = 4
    var image : UIImageView?
    var imageCenterPoint : CGPoint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.didBecomeActiveNotification, object: nil)

        
        if imagesArray.count == 0{
            [imgView1,imgView2,imgView3,imgView4,imgViewSinglePic].forEach({
                $0?.isHidden = true
            })
            
            DisplayBanner.infoBanner(message: "No Images to show")
            self.popViewController()
            
            return
        }

        // Do any additional setup after loading the view.
        view1.makeRoundCorner(10.0)
        view2.makeRoundCorner(10.0)
        view3.makeRoundCorner(10.0)
        view4.makeRoundCorner(10.0)
        
        imgView1.makeRoundCorner(10.0)
        imgView2.makeRoundCorner(10.0)
        imgView3.makeRoundCorner(10.0)
        imgView4.makeRoundCorner(10.0)
        
        imgViewSinglePic.makeRoundCorner(10.0)
        viewSinglePicture.makeRoundCorner(10.0)
        
        view1.makeBorder1(2.0, color: .white)
        view2.makeBorder1(2.0, color: .white)
        view3.makeBorder1(2.0, color: .white)
        view4.makeBorder1(2.0, color: .white)
        viewSinglePicture.makeBorder1(2.0, color: .white)
        
        view1.clipsToBounds = false
        view2.clipsToBounds = false
        view3.clipsToBounds = false
        view4.clipsToBounds = false
        viewSinglePicture.clipsToBounds = false
        
        imgView1.tag = 101
        imgView2.tag = 102
        imgView3.tag = 103
        imgView4.tag = 104
        imgViewSinglePic.tag = 105
        
        self.remainingImagesArray = self.imagesArray
        
        switch selectedBtnCount {
            
        case 1:
            print("Fine")
        case 2:
            imagesArray.count >= 2 ? print("Fine"): (selectedBtnCount = 1)
        case 3:
            imagesArray.count >= 3 ? print("Fine"): (selectedBtnCount = imagesArray.count)
        case 4:
            imagesArray.count >= 4 ? print("Fine"): (selectedBtnCount = imagesArray.count)
        default:
            print("default case for selected btn count")
        }
        
//        for i in 0 ..< selectedBtnCount {
//
//        }
//
        for _ in 0 ..< selectedBtnCount {
            //print(i)
            self.remainingImagesArray.remove(at: 0)
            print("Final Array is \(remainingImagesArray.count)")
        }
        
        setPictures()
        
        let panGesture1 = UIPanGestureRecognizer(target: self, action:(#selector(self.handleGesture(_:))))
        let panGesture2 = UIPanGestureRecognizer(target: self, action:(#selector(self.handleGesture(_:))))
        let panGesture3 = UIPanGestureRecognizer(target: self, action:(#selector(self.handleGesture(_:))))
        let panGesture4 = UIPanGestureRecognizer(target: self, action:(#selector(self.handleGesture(_:))))
        let panGesture5 = UIPanGestureRecognizer(target: self, action:(#selector(self.handleGesture(_:))))

        self.imgView1.addGestureRecognizer(panGesture1)
        self.imgView2.addGestureRecognizer(panGesture2)
        self.imgView3.addGestureRecognizer(panGesture3)
        self.imgView4.addGestureRecognizer(panGesture4)
        self.imgViewSinglePic.addGestureRecognizer(panGesture5)
        
        let mytapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer1.numberOfTapsRequired = 1
        
        let mytapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer2.numberOfTapsRequired = 1
        
        let mytapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer3.numberOfTapsRequired = 1
        
        let mytapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer4.numberOfTapsRequired = 1
        
        let mytapGestureRecognizer5 = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer5.numberOfTapsRequired = 1
        
        self.imgView1.addGestureRecognizer(mytapGestureRecognizer1)
        self.imgView2.addGestureRecognizer(mytapGestureRecognizer2)
        self.imgView3.addGestureRecognizer(mytapGestureRecognizer3)
        self.imgView4.addGestureRecognizer(mytapGestureRecognizer4)
        self.imgViewSinglePic.addGestureRecognizer(mytapGestureRecognizer5)

    }
    
    @objc func appMovedToForeground() {
        
    }
    
    func setPictures() {
        switch selectedBtnCount {
            
        case 1:
            
            viewContainer.isHidden = true
            viewSinglePicture.isHidden = false
            self.imgViewSinglePic.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray.first?.selectedImgString ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
        case 2:
            viewSinglePicture.isHidden = true
            
            imgView1.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray.first?.selectedImgString ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            imgView2.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray[1].selectedImgString)".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
            stckVwSecondRow.isHidden = true
            
        case 3:
            viewSinglePicture.isHidden = true
            
            view4.isHidden = true
            imgView1.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray.first?.selectedImgString ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
            imgView2.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray[1].selectedImgString)".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
            imgView3.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray[2].selectedImgString)".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
        case 4:
            viewSinglePicture.isHidden = true
            
            imgView1.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray.first?.selectedImgString ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
            imgView2.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray[1].selectedImgString)".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
            imgView3.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray[2].selectedImgString)".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
            imgView4.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(imagesArray[3].selectedImgString)".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
        default:
            print("")
        }
    }
    
    @objc func myTapAction(recognizer: UITapGestureRecognizer) {
        let image = recognizer.view as! UIImageView
        
        selectedViewBorder(tagValue: image.tag, addBorder: true)
        //selectedUserInteraction(tagValue: image.tag, disableOtherImageInteraction: true)
    }
    
    @objc func handleGesture(_ panGesture: UIPanGestureRecognizer) {
        // get translation
        let translation = panGesture.translation(in: view)
        panGesture.setTranslation(CGPoint.zero, in: view)
        print(translation)

        // create a new Label and give it the parameters of the old one
        image = panGesture.view as? UIImageView
        guard let image = self.image else { return }
        image.center = CGPoint(x: image.center.x+translation.x, y: image.center.y+translation.y)
        self.imageCenterPoint = image.center
        image.isMultipleTouchEnabled = false
        image.isUserInteractionEnabled = true
        

        if panGesture.state == UIGestureRecognizer.State.began {
            // add something you want to happen when the Label Panning has started
            print("tag is\(image.tag)")
            //selectedViewBorder(tagValue: image.tag, addBorder: true)
            //selectedUserInteraction(tagValue: image.tag, disableOtherImageInteraction: true)
        }

        if panGesture.state == UIGestureRecognizer.State.ended {
            // add something you want to happen when the Label Panning has ended
            //print("ended translation\(translation)")
            
            //Move label back to original position (function invoked when gesture stops)
            //print("last image center\(image.center)")
            print("last image frame\(image.frame)")
            
            let width = Int(image.frame.width)
            let imageOriginX = abs(Int(image.frame.origin.x))
            let imageOriginY = abs(Int(image.frame.origin.y))
            
            if width - imageOriginX <= width/2{//0{
                print("Do something for X")
                performImageChanges(tagValue: image.tag)
            }
        else if width - imageOriginY <= width/2{//0{
                print("Do something for Y")
                performImageChanges(tagValue: image.tag)
            }
            else{
                print("Do Nothing")
            }
            
            resetWithAnimation()
        }

        if panGesture.state == UIGestureRecognizer.State.changed {
            // add something you want to happen when the Label Panning has been change ( during the moving/panning )
        } else {
            // or something when its not moving
        }
        
    }
    
    func resetWithAnimation()
    {
        guard let image = self.image else { return }
        
        UIView.animate(withDuration: 0.4) {
            //image.center = CGPoint(x: self.view1.bounds.width / 2, y: self.view1.bounds.height / 2)
            
            switch image.tag {
            case 101:
                image.center = CGPoint(x: self.view1.bounds.width / 2, y: self.view1.bounds.height / 2)
            case 102:
                image.center = CGPoint(x: self.view2.bounds.width / 2, y: self.view2.bounds.height / 2)
            case 103:
                image.center = CGPoint(x: self.view3.bounds.width / 2, y: self.view3.bounds.height / 2)
            case 104:
                image.center = CGPoint(x: self.view4.bounds.width / 2, y: self.view4.bounds.height / 2)
            default:
                image.center = CGPoint(x: self.imgViewSinglePic.bounds.width / 2, y: self.imgViewSinglePic.bounds.height / 2)
            }
            
            self.selectedUserInteraction(tagValue: 110, disableOtherImageInteraction: false)
            //self.selectedViewBorder(tagValue: image.tag, addBorder: false)
        }
    }
    
    func selectedViewBorder(tagValue:Int , addBorder:Bool){
        
        switch tagValue {
        case 101:
            addBorder ? view1.makeBorder1(6.0, color: UIColor(hex: "#FFD949")) : self.view1.makeBorder1(2.0, color: .white)
            
            self.view2.makeBorder1(2.0, color: .white)
            self.view3.makeBorder1(2.0, color: .white)
            self.view4.makeBorder1(2.0, color: .white)
            self.viewSinglePicture.makeBorder1(2.0, color: .white)
            
            //Higlights only for two seconds
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.view1.makeBorder1(2.0, color: .white)
            }
        case 102:
            addBorder ? view2.makeBorder1(6.0, color: UIColor(hex: "#FFD949")) : view2.makeBorder1(2.0, color: .white)
            
            self.view1.makeBorder1(2.0, color: .white)
            self.view3.makeBorder1(2.0, color: .white)
            self.view4.makeBorder1(2.0, color: .white)
            self.viewSinglePicture.makeBorder1(2.0, color: .white)
            
            //Higlights only for two seconds
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.view2.makeBorder1(2.0, color: .white)
            }
        case 103:
            addBorder ? view3.makeBorder1(6.0, color: UIColor(hex: "#FFD949")) : self.view3.makeBorder1(2.0, color: .white)
            
            self.view1.makeBorder1(2.0, color: .white)
            self.view2.makeBorder1(2.0, color: .white)
            self.view4.makeBorder1(2.0, color: .white)
            self.viewSinglePicture.makeBorder1(2.0, color: .white)
            
            //Higlights only for two seconds
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.view3.makeBorder1(2.0, color: .white)
            }
        case 104:
            addBorder ? view4.makeBorder1(6.0, color: UIColor(hex: "#FFD949")) : self.view4.makeBorder1(2.0, color: .white)
            
            self.view1.makeBorder1(2.0, color: .white)
            self.view2.makeBorder1(2.0, color: .white)
            self.view3.makeBorder1(2.0, color: .white)
            self.viewSinglePicture.makeBorder1(2.0, color: .white)
            
            //Higlights only for two seconds
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.view4.makeBorder1(2.0, color: .white)
            }
        case 105:
            addBorder ? viewSinglePicture.makeBorder1(6.0, color: UIColor(hex: "#FFD949")) : self.viewSinglePicture.makeBorder1(2.0, color: .white)
            
            self.view1.makeBorder1(2.0, color: .white)
            self.view2.makeBorder1(2.0, color: .white)
            self.view3.makeBorder1(2.0, color: .white)
            self.view4.makeBorder1(2.0, color: .white)
            
            
            //Higlights only for two seconds
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.viewSinglePicture.makeBorder1(2.0, color: .white)
            }
        default:
            print("default")
        }
        
    }
    
    func selectedUserInteraction(tagValue:Int , disableOtherImageInteraction interaction :Bool){
    
        switch tagValue {
        case 101:
            imgView1.isUserInteractionEnabled = interaction
            imgView2.isUserInteractionEnabled = !interaction
            imgView3.isUserInteractionEnabled = !interaction
            imgView4.isUserInteractionEnabled = !interaction
        case 102:
            imgView1.isUserInteractionEnabled = !interaction
            imgView2.isUserInteractionEnabled = interaction
            imgView3.isUserInteractionEnabled = !interaction
            imgView4.isUserInteractionEnabled = !interaction
        case 103:
            imgView1.isUserInteractionEnabled = !interaction
            imgView2.isUserInteractionEnabled = !interaction
            imgView3.isUserInteractionEnabled = interaction
            imgView4.isUserInteractionEnabled = !interaction
        case 104:
            imgView1.isUserInteractionEnabled = !interaction
            imgView2.isUserInteractionEnabled = !interaction
            imgView3.isUserInteractionEnabled = !interaction
            imgView4.isUserInteractionEnabled = interaction
        default:
            imgView1.isUserInteractionEnabled = true
            imgView2.isUserInteractionEnabled = true
            imgView3.isUserInteractionEnabled = true
            imgView4.isUserInteractionEnabled = true
        }
    }
    
    func performImageChanges(tagValue:Int){
    
        if remainingImagesArray.count <= 0{
            print("Do Nothing")
            return
        }
        else{
            switch tagValue {
            case 101:
                self.swapImages(0,imageView: imgView1)
                
            case 102:
                self.swapImages(1,imageView: imgView2)
                
            case 103:
                self.swapImages(2,imageView: imgView3)
                
            case 104:
                self.swapImages(3,imageView: imgView4)
                
            case 105:
                self.swapImages(0,imageView: imgViewSinglePic)
                
            default:
                print("")
            }
        }
        
        
    }
    
    func swapImages(_ index :Int, imageView:UIImageView){
        let temp = imagesArray[index]
        
        let randomNumber = Int(arc4random_uniform(UInt32(remainingImagesArray.count)))
        imagesArray[index] = remainingImagesArray[randomNumber]
        
        imageView.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(remainingImagesArray[randomNumber].selectedImgString)".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
        
        remainingImagesArray.remove(at: randomNumber)
        remainingImagesArray.append(temp)
        
        
    }
    
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        case homeBtn:
            self.navigationController?.popToViewController(ofClass: HomeVC.self)
        default:
            break
        }
    }

}

extension UIView{
    func makeBorder1(_ width:CGFloat,color:UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        //self.clipsToBounds = true
    }

}

