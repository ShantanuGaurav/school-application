//
//  LoginVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var forgotBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var warningBtn: UIButton!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
 
    var viewModel = LoginVM()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        viewModel.delegate = self
        
        forgotBtn.setTitle("", for: .normal)
        signupBtn.setTitle("", for: .normal)
        warningBtn.setTitle("", for: .normal)
        emailView.makeBorder(1, color: Color.appBorderColor)
        passwordView.makeBorder(1, color: Color.appBorderColor)
        
        //loginBtn.makeRoundCorner(40)
        mainView.makeRoundCorner(10)
        emailView.makeRoundCorner(10)
        passwordView.makeRoundCorner(10)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loginBtn.makeRoundCorner(loginBtn.frame.height/2)
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case loginBtn:
            validation()
        case forgotBtn:
            let vc = ForgotPasswordVC.instantiate(fromAppStoryboard: .Registration)
            self.navigationController?.pushViewController(vc, animated: true)
        case signupBtn:
            let vc = SignupVC.instantiate(fromAppStoryboard: .Registration)
            self.navigationController?.pushViewController(vc, animated: true)
        case warningBtn:
            let vc = InfoVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    // MARK: - Validation
    private func validation() {
        guard let emailAddress = emailTxt.text?.trim, !emailAddress.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.enterEmail)
            return
        }
        guard Common.instance.isValidEmail(testStr: emailTxt.text!) else{
            DisplayBanner.infoBanner(message:Constant.validEmailId)
            return
        }
        guard let password = passwordTxt.text, !password.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.password)
            return
        }
        if password.count < 6 || password.count > 15 {
            DisplayBanner.infoBanner(message:Constant.invalidPassword)
            return
        }
        
        let params : [String : Any] = [
            "email":emailAddress,
            "password":password
        ]
        viewModel.login(params: params)
    }
}
