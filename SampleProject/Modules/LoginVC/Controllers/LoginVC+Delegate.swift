//
//  LoginVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import Foundation

extension LoginVC : LoginProtocolDelegate {
    
    func loginSuccesfully() {
        let vc = HomeVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
