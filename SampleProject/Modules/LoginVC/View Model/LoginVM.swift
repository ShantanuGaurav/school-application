//
//  LoginVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

protocol LoginProtocolDelegate : AnyObject {
    func loginSuccesfully()
}

import PromiseKit

class LoginVM {
    
    weak var delegate : LoginProtocolDelegate?
    
    // MARK: - Login API
    func login(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.login(params: params)
        }.done({ (model: LoginModel) in
            self.handleResponseLogin(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Login API
    func handleResponseLogin(model: LoginModel) {
        if model.status == 200  || model.status == 201 {
            kUserDefaults.set(model.data?.token, forKey: AppKeys.token)
            kAppDelegate.savedProfileData = model.data
            self.delegate?.loginSuccesfully()
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
