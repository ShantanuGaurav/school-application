//
//  LoginModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let status: Int?
    let message: String?
    let data: LoginData?
}

// MARK: - LoginData
struct LoginData: Codable {
    let user: LoginUser?
    let token: String?
}

// MARK: - LoginUser
struct LoginUser: Codable {
    let id: Int?
    let name, email, isVerified: String?
    let emailVerifiedAt: String?
    let isLoggedIn: Int?
    let lastLoginAt: String?
    let status: String?
    let schoolID: String?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name, email
        case isVerified = "is_verified"
        case emailVerifiedAt = "email_verified_at"
        case isLoggedIn = "is_logged_in"
        case lastLoginAt = "last_login_at"
        case status
        case schoolID = "school_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
