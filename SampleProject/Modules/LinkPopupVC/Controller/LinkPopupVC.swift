//
//  LinkPopupVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit

class LinkPopupVC: UIViewController {

    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    var dissmissCallBack : (() -> ())?
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.view.isOpaque = false
        mainView.makeRoundCorner(20)
        mainView.dropShadow()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        okBtn.makeRoundCorner(okBtn.frame.height/2)
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButton(_ sender: UIButton) {
        switch sender {
        case okBtn:
            self.dismiss(animated: true) {
                self.dissmissCallBack?()
            }
        default:
            break
        }
    }
}
