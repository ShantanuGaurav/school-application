//
//  SignupVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit
import Foundation

// MARK: - SignupVC Protocol Delegate

extension SignupVC : SignupProtocolDelegate, SignupProtocol {
    
    // MARK: - Tap on Signup
    func tapOnSignUp(name: String?, email: String?, password: String?, confirmPassword: String?) {
        guard let fullname = name?.trim, !fullname.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.enterName)
            return
        }
        if fullname.count > 40 {
            DisplayBanner.infoBanner(message:Constant.nameMaxChar)
            return
        }
        guard let emailAddress = email?.trim, !emailAddress.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.enterEmail)
            return
        }
        guard Common.instance.isValidEmail(testStr: email!) else{
            DisplayBanner.infoBanner(message:Constant.validEmailId)
            return
        }
        guard let password = password, !password.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.password)
            return
        }
        
        if password.count < 6 || password.count > 15 {
            DisplayBanner.infoBanner(message:Constant.passwordLength)
            return
        }
        
        guard let confirPassword = confirmPassword, !confirPassword.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.confirmPassword)
            return
        }
        
        if password != confirPassword {
            DisplayBanner.infoBanner(message:Constant.passwordMatch)
            return
        }
        
        let params : [String : Any] = [
            "name":fullname,
            "email":emailAddress,
            "password":confirPassword
        ]
        viewModel.signup(params: params)
    }
    
    func signupSuccesfully() {
        let vc = HomeVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
