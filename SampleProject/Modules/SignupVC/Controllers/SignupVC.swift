//
//  SignupVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit

class SignupVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var warningBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel = SignupVM()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        viewModel.delegate = self
        tableView.separatorColor = .clear
        backBtn.setTitle("", for: .normal)
        loginBtn.setTitle("", for: .normal)
        warningBtn.setTitle("", for: .normal)
        tableView.registerCell(with: SignupCell.self)
        
        mainView.makeRoundCorner(10)
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        case loginBtn:
            self.navigationController?.popViewController(animated: true)
        case warningBtn:
            let vc = InfoVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
}
