//
//  SignupCell.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit

protocol SignupProtocolDelegate : AnyObject {
    func tapOnSignUp(name : String?, email : String?, password : String?, confirmPassword : String?)
}

class SignupCell: UITableViewCell {

    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmpasswordTxt: UITextField!
 
    weak var delegate : SignupProtocolDelegate?
    
    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
   
    // MARK: - Initial Setup
    private func initialSetup() {
        selectionStyle = .none
        
        nameTxt.autocapitalizationType = .sentences

        nameView.makeBorder(1, color: Color.appBorderColor)
        emailView.makeBorder(1, color: Color.appBorderColor)
        passwordView.makeBorder(1, color: Color.appBorderColor)
        confirmPasswordView.makeBorder(1, color: Color.appBorderColor)
        
        nameView.makeRoundCorner(10)
        emailView.makeRoundCorner(10)
        passwordView.makeRoundCorner(10)
        confirmPasswordView.makeRoundCorner(10)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        signupBtn.makeRoundCorner(signupBtn.frame.height/2)
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case signupBtn:
            self.delegate?.tapOnSignUp(name: nameTxt.text, email: emailTxt.text, password: passwordTxt.text, confirmPassword: confirmpasswordTxt.text)
        default:
            break
        }
    }
}
