//
//  SignupVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import PromiseKit

protocol SignupProtocol : AnyObject {
    func signupSuccesfully()
}

class SignupVM {
    
    weak var delegate : SignupProtocol?
    
    // MARK: - Signup API
    func signup(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.register(params: params)
        }.done({ (model: LoginModel) in
            self.handleResponseSignup(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }
    
    // MARK: - Handle Response of Signup API
    func handleResponseSignup(model: LoginModel) {
        if model.status == 200  || model.status == 201 {
            kUserDefaults.set(model.data?.token, forKey: AppKeys.token)
            kAppDelegate.savedProfileData = model.data
            self.delegate?.signupSuccesfully()
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
    
}
