//
//  CategoriesVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 09/02/22.
//

import PromiseKit

protocol CategoriesProtocolDelegate : AnyObject {
    func parseCategoriesListData(data : CategoriesListModel, page :Int, searchString : String)
}

class CategoriesVM {
    
    weak var delegate : CategoriesProtocolDelegate?
    
    // MARK: - Categories List API
    func categoriesList(params: [String:Any], page:Int, searchString : String) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.categorylist(params: params)
        }.done({ (model: CategoriesListModel) in
            self.handleResponseCategoriesList(model: model, page: page, searchString : searchString)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Categories List API
    func handleResponseCategoriesList(model: CategoriesListModel, page:Int, searchString : String) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.parseCategoriesListData(data: model, page: page, searchString: searchString)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
