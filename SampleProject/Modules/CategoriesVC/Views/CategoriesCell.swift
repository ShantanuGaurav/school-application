//
//  CategoriesCell.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit

class CategoriesCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
   
    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.makeRoundCorner(12)
    }
    
    // MARK: - Populate Categories List Data
    func populateCategoriesListData(model : CategoriesList) {
        nameLbl.text = model.categoryName ?? ""
        imgView.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(model.imagePath ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
    }
}
