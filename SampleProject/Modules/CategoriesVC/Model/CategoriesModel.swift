//
//  CategoriesModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 09/02/22.
//

/*
import Foundation

// MARK: - CategoriesListModel
struct CategoriesListModel: Codable {
    let status: Int?
    let message: String?
    let data: CategoriesListData?
}

// MARK: - CategoriesListData
struct CategoriesListData: Codable {
    let category: [CategoriesList]?
}

// MARK: - CategoriesList
struct CategoriesList: Codable {
    let imagePath: String?
    let id: Int?
    let createdAt, categoryName: String?
    let subfolderID: Int?
    let isDeleted, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case imagePath = "image_path"
        case id
        case createdAt = "created_at"
        case categoryName = "category_name"
        case subfolderID = "subfolder_id"
        case isDeleted = "is_deleted"
        case updatedAt = "updated_at"
    }
}
*/

import Foundation

// MARK: - CategoriesListModel
struct CategoriesListModel: Codable {
    let status: Int?
    let message: String?
    var data: CategoriesListDataClass?
}

// MARK: - DataClass
struct CategoriesListDataClass: Codable {
    var category: CategoriesListCategory?
}

// MARK: - Category
struct CategoriesListCategory: Codable {
    let currentPage: Int?
    var data: [CategoriesList]?
    let firstPageURL: String?
    let from, lastPage: Int?
    let lastPageURL: String?
    //let links: [Link]?
    let nextPageURL: String?
    let path: String?
    let perPage: Int?
    let prevPageURL: String?
    let to, total: Int?

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case data
        case firstPageURL = "first_page_url"
        case from
        case lastPage = "last_page"
        case lastPageURL = "last_page_url"
        //case links
        case nextPageURL = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageURL = "prev_page_url"
        case to, total
    }
}

// MARK: - Datum
struct CategoriesList: Codable {
    let id: Int?
    let categoryName: String?
    let subfolderID: Int?
    let imagePath, createdAt, updatedAt, isDeleted: String?
    let categoryOrder: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryName = "category_name"
        case subfolderID = "subfolder_id"
        case imagePath = "image_path"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isDeleted = "is_deleted"
        case categoryOrder = "category_order"
    }
}
