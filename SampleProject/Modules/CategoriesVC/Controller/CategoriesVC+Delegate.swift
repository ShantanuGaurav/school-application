//
//  CategoriesVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 09/02/22.
//

import UIKit
import Foundation

// MARK: - CategoriesVC Protocol Delegate
extension CategorieVC : CategoriesProtocolDelegate {
    
    // MARK: - Parse Categories List Data
    func parseCategoriesListData(data : CategoriesListModel, page : Int, searchString: String) {
        
        self.searchText = self.searchTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        //self.objCategoriesListModel = data
        
        if page == 1{
            self.objCategoriesListModel = data
        }
        else{
            self.pageNumber += 1
            guard let data = data.data?.category?.data else{
                return
            }
            self.objCategoriesListModel?.data?.category?.data! += data
        }
        
        self.collectionView.reloadData()
    }
}
