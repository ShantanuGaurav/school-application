//
//  CategoriesVC+CollectionDelegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit
import Foundation

// MARK: - CategoriesVC Collection Delegate
extension CategorieVC : CollectionDelegate {
    
    // MARK: - Number of item in section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.objCategoriesListModel?.data?.category?.data?.count ?? 0 == 0 ? (collectionView.setEmptyMessage("No Data Available")) : (collectionView.restore())
        return self.objCategoriesListModel?.data?.category?.data?.count ?? 0
    }
    
    // MARK: - Cell for item at
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: CategoriesCell.self, indexPath: indexPath)
        guard let data = self.objCategoriesListModel?.data?.category?.data?[indexPath.row] else {return UICollectionViewCell()}
        cell.populateCategoriesListData(model : data)
        return cell
    }
    
    // MARK: - Did Select Item At
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = SubFolderVC.instantiate(fromAppStoryboard: .Main)
        guard let id = self.objCategoriesListModel?.data?.category?.data?[indexPath.row].id else {return}
        
        var carriedObj = self.carriedDataObj
        carriedObj.catID = id
        vc.carriedDataObj = carriedObj
        
        vc.passSelectedPictureIdsArray = {[weak self] ids in
            self?.carriedDataObj.selectedImgID = ids
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row == (self.objCategoriesListModel?.data?.category?.data?.count ?? 0) - 1 {  //numberofitem count
            if self.pageNumber < (self.objCategoriesListModel?.data?.category?.lastPage ?? 1){
                self.hitServiceCategoryList(page: pageNumber + 1, search: searchText)
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 75) / 3.0 , height: ((collectionView.frame.width - 50) / 3.0) * 1.04)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
}
