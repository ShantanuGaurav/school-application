//
//  CategorieVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit

class CategorieVC: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel = CategoriesVM()
    var objCategoriesListModel : CategoriesListModel?
    var pageNumber = 1
    var searchText = ""
    var carriedDataObj = SelectedDataStruct()
    var passSelectedPictureIdsArray : ((_ ids :[SelectedImageStruct])->())?
    
    //Pull to Refresh
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                                    #selector(refreshPage(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.white
        return refreshControl
    }()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - View will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        viewModel.delegate = self
        self.searchTxt.delegate = self
        searchView.makeRoundCorner(34)
        backBtn.setTitle("", for: .normal)
        homeBtn.setTitle("", for: .normal)
        collectionView.registerCell(with: CategoriesCell.self)
        collectionView.refreshControl = refreshControl
        searchTxt.setupRightImage(imageName: "Icon ionic-ios-search")
        self.searchView.makeBorder(1, color: Color.searchBorderColor)
        
        self.hitServiceCategoryList()
    }
    
    func hitServiceCategoryList(page : Int = 1, search : String = ""){
        let params : [String : Any] = [
            "subfolder_id": self.carriedDataObj.catID, //1
            "page": "\(page)",
            "search": "\(search)"
        ]
        viewModel.categoriesList(params: params, page: page, searchString: search)
    }
    
    //Pull to Refresh
    @objc func refreshPage(_ refreshControl: UIRefreshControl) {
        //Hit Service
        self.pageNumber = 1
        self.searchTxt.text = ""
        self.searchText = ""
        self.hitServiceCategoryList()
        refreshControl.endRefreshing()
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.passSelectedPictureIdsArray?(carriedDataObj.selectedImgID)
            self.navigationController?.popViewController(animated: true)
        case homeBtn:
            self.navigationController?.popToViewController(ofClass: HomeVC.self)
        default:
            break
        }
    }
}
