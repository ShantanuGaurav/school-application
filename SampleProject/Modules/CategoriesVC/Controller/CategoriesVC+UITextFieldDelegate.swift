//
//  CategoriesVC+UITextFieldDelegate.swift
//  SampleProject
//
//  Created by Shivam Garg on 21/03/22.
//

import Foundation
import UIKit

extension CategorieVC : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if text == searchText{
            return
        }
        else{
            self.pageNumber = 1
            self.hitServiceCategoryList(page: self.pageNumber, search: text ?? "")
        }
    }
    
}
