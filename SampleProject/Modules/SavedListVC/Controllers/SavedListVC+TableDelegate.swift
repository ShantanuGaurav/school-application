//
//  SavedListVC+TableDelegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 04/02/22.
//

import UIKit
import Foundation

// MARK: - Saved list VC Table Delegate
extension SavedListVC : TableDelegate {
    
    // MARK: - Number of row in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.objSavedListModel?.data?.data?.count ?? 0 == 0 ? (tableView.setEmptyMessage("No Data Available")) : (tableView.restore())
        return self.objSavedListModel?.data?.data?.count ?? 0
    }
    
    // MARK: - Cell for row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: SavedListCell.self)
        
        guard let model = self.objSavedListModel?.data?.data?[indexPath.row] else {return UITableViewCell()}
        cell.populateSavedList(data : model)
        cell.editBtn.addTarget(self, action: #selector(tapOnEdit(sender:)), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(tapOnDelete(sender:)), for: .touchUpInside)
        cell.startBtn.addTarget(self, action: #selector(tapOnStart(sender:)), for: .touchUpInside)
        cell.viewBtn.addTarget(self, action: #selector(tapOnView(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("willDisplay cell \(indexPath.row)")
        if indexPath.row == (self.objSavedListModel?.data?.data?.count ?? 0) - 1 {  //numberofitem count
            if self.pageNumber < (self.objSavedListModel?.data?.lastPage ?? 1) && !isDataLoading{
                self.hitServiceToGetSavedList(page: pageNumber + 1, search: searchText)
            }
        }
    }
    
    // MARK: - Height for row at
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - Tap On Delete
    @objc func tapOnDelete(sender: UIButton) {
        
        
        guard let indexPath = self.tableView.getIndexPath(sender) else  {return}
        
        guard let id = self.objSavedListModel?.data?.data?[indexPath.row].id else {return}
        
        self.presentAlertViewWithTwoButtons(alertTitle: "", alertMessage: Constant.deleteFromSavedList, btnOneTitle: Constant.yes, btnOneTapped: { UIAlertAction in
            let params : [String : Any] = [
                "id":id
            ]
            self.viewModel.deleteSavedList(params: params, deletedIndex: indexPath.row)
        }, btnTwoTitle: Constant.cancel, btnTwoTapped: { UIAlertAction in
            print("Cancel")
        })
    }
    
    // MARK: - Tap On Edit
    @objc func tapOnEdit(sender: UIButton) {
        /*
        let vc = SubFolderVC.instantiate(fromAppStoryboard: .Main)
        vc.isEdit = true
        vc.catID = self.objSavedListModel?.data?.data?[sender.tag].id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
        */
        guard let indexPath = self.tableView.getIndexPath(sender) else  {return}
        
        guard let data = self.objSavedListModel?.data?.data?[indexPath.row] else {return}
        
        let vc = SuperFolderVC.instantiate(fromAppStoryboard: .Main)
        if PictureGroupEnum.Themes.rawValue == data.defaultFolderID {
            vc.carriedDataObj.selectedPictureGroup = .Themes
        }
        else{
            vc.carriedDataObj.selectedPictureGroup = .FirstWords
        }
        
        vc.carriedDataObj.selectedImgID = data.images?.map({
            
            SelectedImageStruct(selectedImgID: $0.imageID ?? -1, selectedImgString: $0.imagePath ?? "")
            
        }) ?? []
        
        vc.carriedDataObj.selectedBtnCount = data.selectedCountForStart ?? -1
        vc.carriedDataObj.collectionType = .Update
        vc.carriedDataObj.collectionID = data.id ?? -1
        vc.carriedDataObj.collectionName = data.collectionName ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Tap On Start
    @objc func tapOnStart(sender: UIButton) {
        guard let indexPath = self.tableView.getIndexPath(sender) else  {return}
        
        guard let data = self.objSavedListModel?.data?.data?[indexPath.row] else {return}
        
        let vc = StartResultVC.instantiate(fromAppStoryboard: .Main)
        vc.selectedBtnCount = data.selectedCountForStart ?? -1
        vc.imagesArray = data.images?.map({
            
            SelectedImageStruct(selectedImgID: $0.imageID ?? -1, selectedImgString: $0.imagePath ?? "")
            
        }) ?? []
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Tap On Start
    @objc func tapOnView(sender: UIButton) {
        guard let indexPath = self.tableView.getIndexPath(sender) else  {return}
        
        guard let data = self.objSavedListModel?.data?.data?[indexPath.row] else {return}
        
        let vc = ViewImagesVC.instantiate(fromAppStoryboard: .Main)
        vc.imagesArray = data.images
        vc.collectionName = data.collectionName ?? ""
        vc.collectionId = data.id ?? -1
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
