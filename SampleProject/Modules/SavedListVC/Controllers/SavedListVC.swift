//
//  SavedListVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 04/02/22.
//

import UIKit

class SavedListVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var isDataLoading = false //Handling for pull to refresh
    var viewModel = SavedListVM()
    var objSavedListModel : SavedListModel?
    var pageNumber = 1
    var searchText = ""
    
    //Pull to Refresh
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                                    #selector(refreshPage(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.white
        return refreshControl
    }()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - View will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        self.viewModel.delegate = self
        self.tableView.separatorColor = .clear
        self.backBtn.setTitle("", for: .normal)
        self.tableView.registerCell(with: SavedListCell.self)
        self.tableView.refreshControl = refreshControl
        self.searchView.makeBorder(1, color: Color.searchBorderColor)
        self.searchTxt.delegate = self
        self.searchTxt.setupRightImage(imageName: "Icon ionic-ios-search")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatedCollectionNotification(notification:)), name: Notification.Name("SendUpdatedCollectionNotification"), object: nil)

        self.hitServiceToGetSavedList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchView.makeRoundCorner(searchView.frame.height/2)
    }
    
    @objc func updatedCollectionNotification(notification: Notification) {
        
        if let collection = notification.object as? [String:Any]{
            if let data = collection["collection"] as? SavedListDatum{
                if let indx = self.objSavedListModel?.data?.data?.firstIndex(where: {$0.id == data.id}) {
                    
                    self.objSavedListModel?.data?.data?[indx] = data
                    
                    self.tableView.reloadRows(at: [IndexPath(row: indx, section: 0)], with: .automatic)
                }
            }
        }
        
    }

    
    func hitServiceToGetSavedList(page : Int = 1, search : String = ""){
        let params : [String : Any] = [
            "page": "\(page)",
            "search": "\(search)"
        ]
        viewModel.getSavedList(params: params, page: page, searchString: search)
    }
    
    //Pull to Refresh
    @objc func refreshPage(_ refreshControl: UIRefreshControl) {
        //Hit Service
        self.isDataLoading = true
        self.pageNumber = 1
        self.searchTxt.text = ""
        self.searchText = ""
        self.hitServiceToGetSavedList()
        refreshControl.endRefreshing()
    }
    
    // MARK: - Tap on Buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
}
