//
//  SavedListVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 11/02/22.
//

import UIKit
import Foundation

// MARK: - Saved List VC Protocol Delegate
extension SavedListVC : SavedListProtocolDelegate {
    
    // MARK: - Reload List
    func reloadList(deletedIndex: Int) {
        self.objSavedListModel?.data?.data?.remove(at: deletedIndex)
        self.tableView.deleteRows(at: [IndexPath(row: deletedIndex, section: 0)], with: .automatic)
    }
    
    // MARK: - Parse Saved List Data
    func parseSavedListData( data : SavedListModel, page: Int, searchString:String) {
        
        self.searchText = self.searchTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if page == 1{
            self.objSavedListModel = data
        }
        else{
            self.pageNumber += 1
            guard let data = data.data?.data else{
                return
            }
            self.objSavedListModel?.data?.data! += data
        }
        
        self.tableView.reloadData()
        self.isDataLoading = false
    }
}
