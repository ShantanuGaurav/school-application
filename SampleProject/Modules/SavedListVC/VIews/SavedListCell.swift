//
//  SavedListCell.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 04/02/22.
//

import UIKit

class SavedListCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionImageView: UIImageView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
 
    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        selectionStyle = .none
        collectionImageView.makeRoundCorner(5)
        mainView.makeRoundCorner(12)
        /*
         img1.makeRoundCorner(10)
         img2.makeRoundCorner(10)
         img3.makeRoundCorner(10)
         img4.makeRoundCorner(10)
         */
        
        editBtn.makeBorder(2, color: Color.appColor)
        deleteBtn.makeBorder(2, color: Color.appColor)
    }
    
    // MARK: - Populate Saved List Data
    func populateSavedList(data : SavedListDatum) {
        self.nameLbl.text = data.collectionName ?? ""
        self.collectionImageView.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(data.images?.first?.imagePath ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
        
        self.dateLbl.text = "\((data.createdAt ?? "").toDateStringFromUTC(inputDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", ouputDateFormat: "dd-MMM-yyyy"))"
        
        /*
        if data.images?.count != 0 {
            self.img1.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(data.images?[0].imagePath ?? "")"), placeholder: UIImage(named: "placeHolder"))
        }
         */
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        editBtn.makeRoundCorner(editBtn.frame.height/2)
        startBtn.makeRoundCorner(startBtn.frame.height/2)
        viewBtn.makeRoundCorner(viewBtn.frame.height/2)
        deleteBtn.makeRoundCorner(deleteBtn.frame.height/2)
    }
}
