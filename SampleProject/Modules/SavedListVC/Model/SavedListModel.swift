//
//  SavedListModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 11/02/22.
//

import Foundation

// MARK: - SavedListModel
struct SavedListModel: Codable {
    let status: Int?
    let message: String?
    var data: SavedListClass?
}

// MARK: - SavedListClass
struct SavedListClass: Codable {
    let lastPageURL: String?
    let prevPageURL: String?
    let from, total: Int?
    let path, firstPageURL: String?
    let lastPage: Int?
    let nextPageURL: String?
    var data: [SavedListDatum]?
    let currentPage: Int?
    //let links: [SavedListLink]?
    let perPage, to: Int?

    enum CodingKeys: String, CodingKey {
        case lastPageURL = "last_page_url"
        case prevPageURL = "prev_page_url"
        case from, total, path
        case firstPageURL = "first_page_url"
        case lastPage = "last_page"
        case nextPageURL = "next_page_url"
        case data
        case currentPage = "current_page"
        //case links
        case perPage = "per_page"
        case to
    }
}

// MARK: - SavedListDatum
struct SavedListDatum: Codable {
    let images: [SavedListImage]?
    let id: Int?
    let createdAt, timeStamp: String?
    let userID: Int?
    let collectionName, updatedAt: String?
    let selectedCountForStart,defaultFolderID: Int?

    enum CodingKeys: String, CodingKey {
        case images, id
        case createdAt = "created_at"
        case timeStamp = "time_stamp"
        case userID = "user_id"
        case collectionName = "collection_name"
        case updatedAt = "updated_at"
        case selectedCountForStart = "selected_count_for_start"
        case defaultFolderID = "default_folder_id"
    }
}

// MARK: - SavedListImage
struct SavedListImage: Codable {
    let imagePath: String?
    let collectionID, id, imageID: Int?
    let createdAt, timeStamp, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case imagePath = "image_path"
        case collectionID = "collection_id"
        case id
        case imageID = "image_id"
        case createdAt = "created_at"
        case timeStamp = "time_stamp"
        case updatedAt = "updated_at"
    }
}

/*
// MARK: - SavedListLink
struct SavedListLink: Codable {
    let url: String?
    let label: String?
    let active: Bool?
}
*/
