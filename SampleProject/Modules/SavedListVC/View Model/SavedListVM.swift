//
//  SavedListVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 11/02/22.
//

import PromiseKit

protocol SavedListProtocolDelegate : AnyObject {
    func reloadList(deletedIndex: Int)
    func parseSavedListData( data : SavedListModel, page: Int, searchString:String)
}

class SavedListVM {
    
    weak var delegate : SavedListProtocolDelegate?
    
    // MARK: - Saved List API
    func getSavedList(params: [String:Any], page:Int, searchString : String) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.getSavedList(params: params)
        }.done({ (model: SavedListModel) in
            self.handleResponsesavedList(model: model, page: page, searchString: searchString)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Saved List API
    func handleResponsesavedList(model: SavedListModel, page:Int, searchString:String) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.parseSavedListData(data: model, page: page, searchString: searchString)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
    
    // MARK: - Delete Saved List API
    func deleteSavedList(params: [String:Any], deletedIndex: Int) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.deleteSavedList(params: params)
        }.done({ (model: SavedListModel) in
            self.handleResponseDeleteSavedList(model: model, deletedIndex: deletedIndex)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Delete Saved List API
    func handleResponseDeleteSavedList(model: SavedListModel, deletedIndex: Int) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.reloadList(deletedIndex: deletedIndex)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
