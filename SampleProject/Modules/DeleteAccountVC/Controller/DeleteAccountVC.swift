//
//  DeleteAccountVC.swift
//  SampleProject
//
//  Created by Dhananjay Kumar on 10/03/23.
//

import UIKit

class DeleteAccountVC: UIViewController {
    @IBOutlet weak var bckButton:UIButton!
    @IBOutlet weak var submitBtn:UIButton!
    @IBOutlet weak var labelDeleteAc:UILabel!
    @IBOutlet weak var labelConfirmPass:UILabel!
    @IBOutlet weak var labelCurrentPass:UILabel!
    @IBOutlet weak var textfieldPass:UITextField!
    @IBOutlet weak var viewTextfield:UIView!


    var viewModel = DeleteAccountVM()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        self.viewModel.completion = { status in
            if status{
                self.setupData()
            }
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        submitBtn.makeRoundCorner(submitBtn.frame.height/2)
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        viewTextfield.makeBorder(1, color: Color.appBorderColor)
        
        //
        viewTextfield.makeRoundCorner(10)
    }

    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case bckButton:
            self.navigationController?.popViewController(animated: true)
        case submitBtn:
            validation()
            
            break
        default:
            break
        }
    }
    
    // MARK: - Validation
    private func validation() {
        guard let currentPassword = textfieldPass.text, !currentPassword.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.currentPassword)
            return
        }
        let params : [String : Any] = [
            "password": currentPassword
        ]
        viewModel.DeleteAccount(params: params)
    }
}
