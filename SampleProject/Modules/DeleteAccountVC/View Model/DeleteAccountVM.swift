//
//  DeleteAccountVM.swift
//  SampleProject
//
//  Created by Dhananjay Kumar on 10/03/23.
//

import PromiseKit

class DeleteAccountVM{
    
    var completion: ((Bool) -> ())?
//    var model: ChangePasswordModel? {
//        didSet{
//            guard let completion = self.completion else{
//                return
//            }
//            completion()
//        }
//    }
    
    // MARK: - Delete Account API
    func DeleteAccount(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.deleteAccount(params: params)
        }.done({ (model: DeleteAccountModel) in
            self.handleResponseChangePassowrd(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }
    
    // MARK: - Handle Response of Change Password API
    func handleResponseChangePassowrd(model: DeleteAccountModel) {
        if model.status == 200  || model.status == 201 {
            //logout()
            guard let completion = completion else{return}
            completion(true)
            
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
