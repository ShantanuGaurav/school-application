//
//  DeleteAccountModel.swift
//  SampleProject
//
//  Created by Dhananjay Kumar on 10/03/23.
//

import UIKit
import Foundation

// MARK: - DeleteAccountModel
struct DeleteAccountModel: Codable {
    let message: String?
    let status: Int?
    let data: DeleteAccountData?
}

// MARK: - DeleteAccountData
struct DeleteAccountData: Codable {
}
