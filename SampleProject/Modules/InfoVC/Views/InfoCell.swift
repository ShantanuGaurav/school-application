//
//  InfoCell.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit

class InfoCell: UITableViewCell {

    @IBOutlet weak var answerLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    
    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        questionLbl.attributedText = "User Instructions:".underLinedString()
        answerLbl.text = "Log In Screen\nFirst-time users will sign up for an account by selecting the “Sign up” link and then will log in.\nReturning users will log in with email address used to sign up and the password created.  If a password is forgotten, it can be reset by selecting the “Forgot Password?” link.\n\nAfter logging into the app, you will be on the Home Screen.\n\nHome Screen (with Home Screen icon)\nSelect the number of pictures you want to display.\n\nWhen you have selected the number of pictures to display, you will be taken to the Select Picture Group Screen automatically.\n\nThroughout the app, you can return to this screen and change the number of pictures to display by selecting the “Home” button.\n\nSelect Picture Group Screen\nSelect the group from which you will choose the specific pictures to display. You may choose pictures from either group you want to display; however, pictures from both groups cannot be displayed simultaneously. "
    }
}

/*
 \n\nIn addition, you may select a previously saved list by selecting the “Saved Lists” button.

 Upon selecting the picture group, you will be taken to the Select Picture Folder Screen automatically; or upon selecting the “Saved Lists” button, you will be taken to the Saved Lists Screen automatically.

 Select Picture Folder Screen
 Select the picture folder from which you want to choose the pictures to display. Pictures from any or all folders may be selected.

 Select Picture Category Screen
 Select the picture category from which you want to choose the pictures to display. Pictures from any or all categories may be selected.

 Select Pictures Screen
 Select all of the the specific pictures you want to display. You may choose as many or as few pictures as appropriate for a given child.

 Selected pictures will be indicated by a check mark.

 Saved Lists Screen
 Shows you all of the lists you have saved.

 On this screen, for a given list you can edit a list, view a list, delete a list, or start targeting the words in a list.

 Selecting the “Edit” button will take you back to the Select Picture Folder Screen from which you can open a given folder, which will take you to the Select Picture Category Screen from which you can select or deselect pictures for a given saved list. You may include as many or as few pictures as necessary from as many picture categories as you like in a given saved list.

 Selecting the “View” button will display all of the pictures you have selected to include in a given saved list.

 Selecting the “Start” button will take you to the final screen where the pictures you selected will be displayed for targeting the chosen vocabulary.

 Save Button
 Allows you to save pictures as a specific list of your creation (e.g., the vocabulary being targeted for a certain child). Selecting the “Save” button will be taken to the “Save List” pop-up where you can give the list you would like to save a specific name (e.g., Amy’s Words). Saving a list or collection of pictures will take you to the Saved Lists screen automatically.

 Start Button
 Automatically takes you to the final screen where the pictures you selected will be displayed for targeting the chosen vocabulary.

 Back Arrow
 On each screen you can return to the previous screen by selecting the Back Arrow.

 Information Icon
 Selecting the lower case i in the circle will provide you with information and instruction for using the app.

 Search Bar
 When on the Select Picture Folder screen, the Select Picture Category screen, or the Select Pictures screen, you can type into the search bar to search either for a given folder, category, or picture by typing the word to search into the search bar and selecting “Done.”

 Other important Information:
 To select a different number of pictures to display, return to the Home Screen by using the Home button.
 To select new pictures from within the same folder, return to the Select Pictures Screen by using the Back Arrow.
 When pictures are being displayed on the final screen, when a picture is selected, a yellow boarder will appear indicating the picture which was chosen.
 To remove one picture from the group displayed and replace it with another one that you previously selected, swipe the picture to the edge of the screen in any direction, and it will be automatically replaced with another picture from those you previously selected.
 To purchase additional picture folders, select the folder you wish to purchase and use your apple ID and password to complete in-app purchases.
 */
