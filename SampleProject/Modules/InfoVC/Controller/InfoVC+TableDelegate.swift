//
//  InfoVC+TableDelegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit
import Foundation

// MARK: - InfoVC Table Delegate
extension InfoVC : TableDelegate {
    
    // MARK: - Number of row in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // MARK: - Cell for row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: InfoCell.self)
        return cell
    }
    
    // MARK: - Height for row at
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
