//
//  InfoVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import WebKit
import UIKit

class InfoVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var webView: WKWebView!
    
   
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
  
    // MARK: - Initial Setup
    private func initialSetup () {
        //tableView.separatorColor = .clear
        backBtn.setTitle("", for: .normal)
        //tableView.registerCell(with: InfoCell.self)
        
        guard let url = URL(string: "http://school.moogcenter.org/info") else {return}
        webView.load(URLRequest(url: url))
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnbuttons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
}
