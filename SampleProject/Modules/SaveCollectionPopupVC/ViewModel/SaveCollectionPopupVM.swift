//
//  SaveCollectionPopupVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 11/02/22.
//

import PromiseKit

protocol SaveCollectionPopupProtocolDelegate : AnyObject {
    func saveSucessfully()
    func updateSucessfully(data: SavedListDatum?)
}

class SaveCollectionPopupVM {
    
    weak var delegate : SaveCollectionPopupProtocolDelegate?
    
    // MARK: - Save Collection List API
    func saveCollection(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.saveCollection(params: params)
        }.done({ (model: CompanySaveColletionsPopupModel) in
            self.handleResponseSaveCollection(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Save Collection List API
    func handleResponseSaveCollection(model: CompanySaveColletionsPopupModel) {
        if model.status == 200  || model.status == 201 {
            DisplayBanner.successBanner(message: model.message)
            self.delegate?.saveSucessfully()
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
    
    // MARK: - Update Collection List API
    func updateCollection(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.updateCollection(params: params)
        }.done({ (model: CompanySaveColletionsPopupModel) in
            self.handleResponseUpdateCollection(model: model, params: params)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Update Collection List API
    func handleResponseUpdateCollection(model: CompanySaveColletionsPopupModel, params:[String:Any]) {
        if model.status == 200  || model.status == 201 {
            DisplayBanner.successBanner(message: model.message)
            self.delegate?.updateSucessfully(data: model.data)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
