//
//  SaveColletionPopupVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 11/02/22.
//

import UIKit
import Foundation

// MARK: - Save Collection Popup VC Protocol Delegate
extension SaveCollectionPopupVC : SaveCollectionPopupProtocolDelegate {

    // MARK: - Save Sucessfully
    func saveSucessfully() {
        self.delegate?.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Update Sucessfully
    func updateSucessfully(data:SavedListDatum?) {
        self.delegate?.backToRoot(data: data)
        self.dismiss(animated: true, completion: nil)
    }
}
