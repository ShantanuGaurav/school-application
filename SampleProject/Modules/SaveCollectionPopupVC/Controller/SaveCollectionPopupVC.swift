//
//  SaveCollectionPopupVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 11/02/22.
//

import UIKit

protocol SavedColletion : AnyObject {
    func reloadData()
    func backToRoot(data :SavedListDatum?)
}

class SaveCollectionPopupVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleNameView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var titleTxt: UITextField!
   
    weak var delegate : SavedColletion?
    var viewModel = SaveCollectionPopupVM()
    var carriedDataObj = SelectedDataStruct()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        self.viewModel.delegate = self
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.view.isOpaque = false
        self.mainView.makeRoundCorner(20)
        self.mainView.dropShadow()
        self.titleNameView.makeBorder(1, color: Color.appBorderColor)
        self.cancelBtn.setTitle("", for: .normal)
        if self.carriedDataObj.collectionType == .Regular{
            self.saveBtn.setTitle("Save", for: .normal)
            self.titleLabel.text = "Save List"
        }
        else{
            self.saveBtn.setTitle("Update", for: .normal)
            self.titleLabel.text = "Update List"
        }
        self.titleTxt.text = self.carriedDataObj.collectionName
        
        titleNameView.makeRoundCorner(10)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        saveBtn.makeRoundCorner(saveBtn.frame.height/2)
    }
    
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case saveBtn:
            validation()
        case cancelBtn:
            self.dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    // MARK: - Validation
    private func validation() {
        guard let title = titleTxt.text?.trim, !title.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.title)
            return
        }
        var params : [String : Any] = [
            "collection_name": title,
            "collection_images": self.carriedDataObj.selectedImgID.map({$0.selectedImgID}),
            "default_folder_id": self.carriedDataObj.selectedPictureGroup.rawValue,
            "selected_count_for_start": self.carriedDataObj.selectedBtnCount,
        ]
        if self.carriedDataObj.collectionType == .Update {
            params["collection_id"] = self.carriedDataObj.collectionID
            viewModel.updateCollection(params: params)
        } else {
            viewModel.saveCollection(params: params)
        }
    }
}
