//
//  SaveColletionPopupModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 11/02/22.
//

import Foundation

// MARK: - CompanySaveColletionsPopupModel
struct CompanySaveColletionsPopupModel: Codable {
    let status: Int?
    let message: String?
    let data: SavedListDatum?
}

// MARK: - CompanySaveColletionsPopupData
struct CompanySaveColletionsPopupData: Codable {
}
