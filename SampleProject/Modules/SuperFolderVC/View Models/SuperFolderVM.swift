//
//  SuperFolderVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 09/02/22.
//

import PromiseKit

protocol SubFolderListProtocolDelegate : AnyObject{
    func parseSubFolderList(data : SubFolderListModel, page :Int, searchString : String)
    func parseSubscriptionPurchase(data: SubscriptionPurchaseModel)
}

class SuperFolderVM {
    
    weak var delegate : SubFolderListProtocolDelegate?
    
    // MARK: - Sub Folder List API
    func subFolderList(params: [String:Any], page:Int, searchString:String) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.subFolderList(params: params)
        }.done({ (model: SubFolderListModel) in
            self.handleResponseSubFolderList(model: model, page: page, searchString: searchString)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Sub Folder List API
    func handleResponseSubFolderList(model: SubFolderListModel, page:Int, searchString:String) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.parseSubFolderList(data: model, page: page, searchString: searchString)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        
        Console.log(model)
    }
    
    // MARK: - Sub Folder List API
    func subscriptionPurchase(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.subscriptionPurchase(params: params)
        }.done({ (model: SubscriptionPurchaseModel) in
            self.handleResponseSubscriptionPurchase(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Sub Folder List API
    func handleResponseSubscriptionPurchase(model: SubscriptionPurchaseModel) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.parseSubscriptionPurchase(data: model)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        
        Console.log(model)
    }
}
