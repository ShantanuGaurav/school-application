//
//  SuperFolderVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit

enum PictureGroupEnum:Int{
    case Themes = 1
    case FirstWords = 2
}

class SuperFolderVC: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var restoreBtn: UIButton!
    @IBOutlet weak var termsOfServiceBtn: UIButton!
    @IBOutlet weak var privacyPolicyBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var pageNumber = 1
    var viewModel = SuperFolderVM()
    var objSubFolderListModel : SubFolderListModel?
    
    var carriedDataObj = SelectedDataStruct()
    var searchText = ""
    
    //Pull to Refresh
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                                    #selector(refreshPage(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.white
        return refreshControl
    }()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        IAPManager.shared.delegate = self
        
    }
    
    // MARK: - View will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        restoreBtn.makeRoundCorner(restoreBtn.height/2.0)
        searchView.makeRoundCorner(searchView.height/2.0)
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
//        if let data = kUserDefaults.array(forKey: AppKeys.purchasedFolders) as? [String]{
//            kAppDelegate.purchasedIds = data
//        }
        self.viewModel.delegate = self
        self.searchTxt.delegate = self
        self.backBtn.setTitle("", for: .normal)
        self.homeBtn.setTitle("", for: .normal)
        self.searchView.makeBorder(1, color: Color.searchBorderColor)
        self.collectionView.registerCell(with: SuperFolderCollCell.self)
        self.collectionView.refreshControl = refreshControl
        self.searchTxt.setupRightImage(imageName: "Icon ionic-ios-search")
        
        if carriedDataObj.selectedPictureGroup == .FirstWords{
            searchViewHeight.constant = 0
            searchView.isHidden = true
        }
        else{
            searchViewHeight.constant = 68
            searchView.isHidden = false
        }
        
        self.hitServiceSubModelList(page: pageNumber)
    }
    
    func hitServiceSubModelList(page : Int = 1, search : String = ""){
        let params : [String : Any] = [
            "folder_id": carriedDataObj.selectedPictureGroup.rawValue,
            "page": "\(page)",
            "search": "\(search)"
        ]
        viewModel.subFolderList(params: params, page: page, searchString: search)
    }
    
    //Pull to Refresh
    @objc func refreshPage(_ refreshControl: UIRefreshControl) {
        //Hit Service
        self.pageNumber = 1
        self.searchTxt.text = ""
        self.searchText = ""
        self.hitServiceSubModelList()
        refreshControl.endRefreshing()
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        case homeBtn:
            self.navigationController?.popToViewController(ofClass: HomeVC.self)
        case restoreBtn:
            IAPManager.shared.restorePurchase()
        case termsOfServiceBtn:
            print("terms of service")
            let url = URL(string: "http://school.moogcenter.org/terms-condition")!
            if UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url)
            }
        case privacyPolicyBtn:
            print("privacy policy")
            let url = URL(string: "http://school.moogcenter.org/privacy-policy")!
            if UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url)
            }
        default:
            break
        }
    }
}
