//
//  SuperFolderVc+UITextFieldDelegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 09/02/22.
//

import UIKit
import Foundation

extension SuperFolderVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if text == searchText{
            return
        }
        else{
            self.pageNumber = 1
            self.hitServiceSubModelList(page: self.pageNumber, search: text ?? "")
        }
    }
}
