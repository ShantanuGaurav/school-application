//
//  SuperFolderVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 09/02/22.
//

import UIKit
import Foundation

// MARK: - Sub Folder List Protocol Delegate
extension SuperFolderVC : SubFolderListProtocolDelegate {
    
    func parseSubscriptionPurchase(data: SubscriptionPurchaseModel) {
        print("Success From Backend")
        
        guard let puchaseIdsArray = data.data?.subscriptiondata?.receipt?.inApp?.map({$0.productID ?? ""}) else { return }
        
        //kAppDelegate.purchasedIds = puchaseIdsArray
        
        //guard let subFolderData = self.objSubFolderListModel?.data?.subfolder?.data else { return }
        
        /*self.objSubFolderListModel?.data?.subfolder?.data = subFolderData.map({
            var dict = $0
            if puchaseIdsArray.contains(dict.inappPurchaseID ?? ""){
                dict.isPurchased = "1"
            }
            return dict
        })
        */
        
        kUserDefaults.setValue(puchaseIdsArray, forKey: AppKeys.purchasedFolders)
        
        if let index = objSubFolderListModel?.data?.subfolder?.data?.firstIndex(where: {$0.inappPurchaseID == data.data?.subscription_id}){
            objSubFolderListModel?.data?.subfolder?.data?[index].isPurchased = "1"
        }
        self.collectionView.reloadData()
        
       // self.objSubFolderListModel?.data?.subfolder?.data = data
        
    }
    
    
    // MARK: - Parsing Sub Folder Data
    func parseSubFolderList(data : SubFolderListModel, page : Int, searchString: String) {
        
        self.searchText = self.searchTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        if page == 1{
            self.objSubFolderListModel = data
        }
        else{
            self.pageNumber += 1
            guard let data = data.data?.subfolder?.data else{
                return
            }
            self.objSubFolderListModel?.data?.subfolder?.data! += data
        }
        
        self.collectionView.reloadData()
    }
}
    
extension SuperFolderVC : IAPManagerDelegate {
    func fetched(products: [IAProduct]) {
        print("")
    }
    
    func itemPurchased(params: [String : Any]) {
        viewModel.subscriptionPurchase(params: params)
    }
}
