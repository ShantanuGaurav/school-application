//
//  SuperFolderVC+CollectionDelegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 03/02/22.
//

import UIKit
import Foundation

extension SuperFolderVC : CollectionDelegate {
  
    // MARK: - Number of items in section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.objSubFolderListModel?.data?.subfolder?.data?.count ?? 0 == 0 ? (collectionView.setEmptyMessage("No Data Available")) : (collectionView.restore())
        return self.objSubFolderListModel?.data?.subfolder?.data?.count ?? 0
    }
    
    // MARK: - Cell for Item at
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: SuperFolderCollCell.self, indexPath: indexPath)
        guard let model = self.objSubFolderListModel?.data?.subfolder?.data?[indexPath.row] else {return cell}
        cell.currentType = carriedDataObj.selectedPictureGroup
        cell.subFolderData = model
        
        cell.nextButtonCallBack = {[weak self] in
            
            guard let strongSelf = self else { return }
            if model.isFree == "1"{
                strongSelf.pushToRespectiveScreens(model.id)
            }
            //MARK: Paid
            else{
                //MARK: purchased
                //kAppDelegate.purchasedIds.contains("\(subFolderData?.inappPurchaseID ?? "")"){//
                if model.isPurchased == "1"{
                    strongSelf.pushToRespectiveScreens(model.id)
                }
                //MARK: not purchased
                else{
                    if let purchaseId = model.inappPurchaseID{
                        IAPManager.shared.purchaseMyProduct(identifier: purchaseId)
                    }else{
                        print("Purchase ID is missing.")
                    }
                }
            }
            //MARK: Free
//            if model.isFree == "1"{
//                strongSelf.pushToRespectiveScreens(model.id)
//            }
//            //MARK: Paid
//            else{
//                //MARK: purchased
//                if kAppDelegate.purchasedIds.contains("\(model.inappPurchaseID ?? "")"){//model.isPurchased == "1"{
//                    strongSelf.pushToRespectiveScreens(model.id)
//                }
//                //MARK: not purchased
//                else{
//                    guard let id = model.inappPurchaseID else { return }
//
//                    let filteredArray = kAppDelegate.subscriptionListObj?.data?.filter({$0.subscriptionID != ""})
//                    let productArray = filteredArray?.map({$0.subscriptionID ?? ""})
//
//                    //Check If the folders is added recently
//                    if productArray?.contains(id) == true {
//                        IAPManager.shared.purchaseMyProduct(identifier: id)
//                        print("Open InApp Purchase")
//                    }
//                    else{
//                        kAppDelegate.getSubscriptionList()
//                    }
//                }
//            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row == (self.objSubFolderListModel?.data?.subfolder?.data?.count ?? 0) - 1 {  //numberofitem count
            if self.pageNumber < (self.objSubFolderListModel?.data?.subfolder?.lastPage ?? 1){
                self.hitServiceSubModelList(page: pageNumber + 1, search: searchText)
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 25) / 2.0 , height: ((collectionView.frame.width - 25) / 2.0) * 1.2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func pushToRespectiveScreens(_ id:Int?){
        guard let id = id else{ return }
        
        //MARK: Open Categories || First Words
        if self.carriedDataObj.selectedPictureGroup == .FirstWords{
            let vc = CategorieVC.instantiate(fromAppStoryboard: .Main)
            
            var carriedObj = self.carriedDataObj
            carriedObj.catID = id
            vc.carriedDataObj = carriedObj
            
            vc.passSelectedPictureIdsArray = {[weak self] ids in
                self?.carriedDataObj.selectedImgID = ids
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        //MARK: Open Images Page || Themes
        else{
            
            let vc = SubFolderVC.instantiate(fromAppStoryboard: .Main)
            
            var carriedObj = self.carriedDataObj
            carriedObj.catID = id
            vc.carriedDataObj = carriedObj
            
            vc.passSelectedPictureIdsArray = {[weak self] ids in
                self?.carriedDataObj.selectedImgID = ids
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
