//
//  SuperFolderCollCell.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 03/02/22.
//

import UIKit

class SuperFolderCollCell: UICollectionViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var imgVIew: UIImageView!
    @IBOutlet weak var lockImgView: UIImageView!
    
    var nextButtonCallBack : (() -> ())?
    var currentType: PictureGroupEnum = .Themes
    var subFolderData :SubFolder? {
        didSet{
            //            if let price = subFolderData?.price{
            //                let value = String(format: "%.2f", price)
            //                priceLbl.text = " ($" + value + ")"
            //
            //            }else{
            //                priceLbl.text = nil
            //
            //            }
            var priceValue: String?
            if let price = subFolderData?.price{
                let value = String(format: "%.2f", price)
                priceValue = "($" + value + ")"
                
            }else{
                priceValue  = nil
                
            }
            priceLbl.text = ""
            priceLbl.isHidden = true
            nameLbl.text = subFolderData?.name ?? ""
            // priceLbl.text = "" //Price
            imgVIew.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(subFolderData?.iconPath ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
            
            //MARK: Free
            if subFolderData?.isFree == "1"{
                lockImgView.isHidden = true
                startBtn.setTitle(currentType == .FirstWords ? "Start Demo": "Start", for: .normal)
            }
            //MARK: Paid
            else{
                //MARK: purchased
                //kAppDelegate.purchasedIds.contains("\(subFolderData?.inappPurchaseID ?? "")"){//
                if subFolderData?.isPurchased == "1"{
                    lockImgView.isHidden = true
                    startBtn.setTitle("Start", for: .normal)
                }
                //MARK: not purchased
                else{
                    lockImgView.isHidden = false
                    
                    startBtn.setTitle(String.join(strings: ["Buy", priceValue], separator: " "), for: .normal)
                }
            }
        }
    }
    
    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.makeRoundCorner(14)
    }

    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case startBtn:
            self.nextButtonCallBack?()
        default:
            break
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.startBtn.makeRoundCorner(self.startBtn.frame.height/2.0)
    }
}

