//
//  SubscriptionPurchaseModel.swift
//  SampleProject
//
//  Created by Shivam Garg on 08/03/22.
//

import Foundation

// MARK: - SubscriptionPurchaseModel
struct SubscriptionPurchaseModel: Codable {
    let status: Int?
    let message: String?
    var data: SubscriptionPurchaseDataClass?
}

// MARK: - DataClass
struct SubscriptionPurchaseDataClass: Codable {
    let subscriptiondata: SubscriptionPurchaseSubscriptiondata?
    let subscription_id: String?
}

// MARK: - Subscriptiondata
struct SubscriptionPurchaseSubscriptiondata: Codable {
    let receipt: SubscriptionPurchaseReceipt?
    let environment: String?
    let latestReceiptInfo: [SubscriptionPurchaseLatestReceiptInfo]?

    enum CodingKeys: String, CodingKey {
        case receipt, environment
        case latestReceiptInfo = "latest_receipt_info"
    }
}

// MARK: - LatestReceiptInfo
struct SubscriptionPurchaseLatestReceiptInfo: Codable {
    let quantity, productID, transactionID, originalTransactionID: String?
    let purchaseDate, purchaseDateMS, purchaseDatePst, originalPurchaseDate: String?
    let originalPurchaseDateMS, originalPurchaseDatePst, isTrialPeriod, inAppOwnershipType: String?

    enum CodingKeys: String, CodingKey {
        case quantity
        case productID = "product_id"
        case transactionID = "transaction_id"
        case originalTransactionID = "original_transaction_id"
        case purchaseDate = "purchase_date"
        case purchaseDateMS = "purchase_date_ms"
        case purchaseDatePst = "purchase_date_pst"
        case originalPurchaseDate = "original_purchase_date"
        case originalPurchaseDateMS = "original_purchase_date_ms"
        case originalPurchaseDatePst = "original_purchase_date_pst"
        case isTrialPeriod = "is_trial_period"
        case inAppOwnershipType = "in_app_ownership_type"
    }
}

// MARK: - Receipt
struct SubscriptionPurchaseReceipt: Codable {
    let receiptType: String?
    let inApp: [SubscriptionPurchaseLatestReceiptInfo]?

    enum CodingKeys: String, CodingKey {
        case receiptType = "receipt_type"
        case inApp = "in_app"
    }
}
