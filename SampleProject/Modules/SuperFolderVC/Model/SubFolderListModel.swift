//
//  SubFolderListModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 09/02/22.
//

import UIKit
import Foundation

// MARK: - SubFolderListModel
struct SubFolderListModel: Codable {
    let status: Int?
    let message: String?
    var data: SubFolderListData?
}

// MARK: - DataClass
struct SubFolderListData: Codable {
    var subfolder: SubfolderPagination?
}

// MARK: - Subfolder
struct SubfolderPagination: Codable {
    let lastPageURL: String?
    let prevPageURL: String?
    let from, total: Int?
    let path, firstPageURL: String?
    let lastPage: Int?
    let nextPageURL: String?
    var data: [SubFolder]?
    let currentPage: Int?
    //let links: [Link]?
    let perPage, to: Int?

    enum CodingKeys: String, CodingKey {
        case lastPageURL = "last_page_url"
        case prevPageURL = "prev_page_url"
        case from, total, path
        case firstPageURL = "first_page_url"
        case lastPage = "last_page"
        case nextPageURL = "next_page_url"
        case data
        case currentPage = "current_page"
        //case links
        case perPage = "per_page"
        case to
    }
}

// MARK: - Datum
struct SubFolder: Codable {
    let iconPath: String?
    let parentFolderID, id: Int?
    let createdAt, isDeleted, path, updatedAt: String?
    let name: String?
    let folderOrder: Int?
    let inappPurchaseID: String?
    let isFree : String? //"0","1"
    let price : Double?
    var isPurchased : String? // "0","1"

    enum CodingKeys: String, CodingKey {
        case iconPath = "icon_path"
        case parentFolderID = "parent_folder_id"
        case id
        case createdAt = "created_at"
        case isDeleted = "is_deleted"
        case path
        case updatedAt = "updated_at"
        case name
        case folderOrder = "folder_order"
        case inappPurchaseID = "inapp_purchase_id"
        case isFree = "is_free"
        case price = "price"
        case isPurchased = "is_purchased"
    }
}

/*
// MARK: - Link
struct Link: Codable {
    let url: String?
    let label: String?
    let active: Bool?
}
*/
