//
//  HomeVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import PromiseKit

protocol LogoutProtocolDelegate : AnyObject {
    func tapOnLogout()
}

class HomeVM {
    
    weak var delegate : LogoutProtocolDelegate?
    
    // MARK: - Logout API
    func logout() {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.logout()
        }.done({ (model: LogoutModel) in
            self.handleResponseLogout(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Logout API
    func handleResponseLogout(model: LogoutModel) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.tapOnLogout()
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
