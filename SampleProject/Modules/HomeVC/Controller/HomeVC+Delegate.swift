//
//  HomeVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import UIKit
import Foundation

extension HomeVC : LogoutProtocolDelegate {
    
    // MARK: - Tap on logout
    func tapOnLogout() {
        kAppDelegate.logoutUser()
    }
    
    /*
    // MARK: - Switch to login screen
    private func switchToLoginScreen() {
        
        let navigation = CWindow?.rootViewController as? UINavigationController
               let controllers  = navigation?.viewControllers
               if controllers?.count == 1{
                   if controllers?.first?.isKind(of: LoginVC.self) ?? false{
                       return
                   }
               }

        kUserDefaults.removeObject(forKey: AppKeys.token)
        let loginVC = LoginVC.instantiate(fromAppStoryboard: .Registration)
        let nav = SwipeableNavigationController(rootViewController: loginVC)
        nav.setNavigationBarHidden(true, animated: false)
    
    }
     */
}
