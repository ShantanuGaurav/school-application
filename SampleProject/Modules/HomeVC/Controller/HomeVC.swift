//
//  HomeVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var popUpVIew: UIView!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var toggleBtn: UIButton!
    @IBOutlet weak var lblDeleteAccount: UILabel!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    
    var showMenu = false
    var viewModel = HomeVM()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - View will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.popUpVIew.alpha = 0.0
        self.popUpVIew.isHidden = true
        self.showMenu = false
        
    }
   
    // MARK: - Initial Setup
    private func initialSetup() {
        
        viewModel.delegate = self
        popUpVIew.makeRoundCorner(8)
        lbl1.textColor = Color.appColor
        lbl2.textColor = Color.appColor
        lbl3.textColor = Color.appColor
        lbl4.textColor = Color.appColor
        view1.makeBorder(2, color: Color.appColor)
        view2.makeBorder(2, color: Color.appColor)
        view3.makeBorder(2, color: Color.appColor)
        view4.makeBorder(2, color: Color.appColor)
    }
    
    override func viewDidLayoutSubviews() {
        view1.makeRounded()
        view2.makeRounded()
        view3.makeRounded()
        view4.makeRounded()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
       {
           let touch = touches.first
           if touch?.view != self.popUpVIew
           {
               self.hideMenu()
           }
       }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case btn1:
            selectedNumbers(btn: btn1)
        case btn2:
            selectedNumbers(btn: btn2)
        case btn3:
            selectedNumbers(btn: btn3)
        case btn4:
            selectedNumbers(btn: btn4)
        case toggleBtn:
            showAndHideMenu()
        case infoBtn:
            let vc = InfoVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        case changeBtn:
            let vc = ChangePasswordVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        case logoutBtn:
            self.presentAlertViewWithTwoButtons(alertTitle: "", alertMessage: "Are you sure you want to logout?", btnOneTitle: "Yes", btnOneTapped: { UIAlertAction in
                self.viewModel.logout()
            }, btnTwoTitle: "No") { UIAlertAction in
                print("NO")
            }
        case btnDeleteAccount:
            let vc = DeleteAccountVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)

        default:
            break
        }
    }
    @IBAction func privacyButtonTapped(_ sender: Any){
       
        print("privacy policy")
        let url = URL(string: "http://school.moogcenter.org/privacy-policy")!
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url)
        }
        self.popUpVIew.alpha = 0.0
        self.popUpVIew.isHidden = true
        self.showMenu = false
    }
    @IBAction func termsButtonTapped(_ sender: Any){
        print("terms of service")
        let url = URL(string: "http://school.moogcenter.org/terms-condition")!
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url)
        }
        self.popUpVIew.alpha = 0.0
        self.popUpVIew.isHidden = true
        self.showMenu = false
    }
    // MARK: - Show and Hide Menu
   private func showAndHideMenu() {
        if showMenu == false {
            self.popUpVIew.alpha = 0.0
            self.popUpVIew.isHidden = false
            self.showMenu = true
            
            UIView.animate(withDuration: 0.6,
                           animations: { [weak self] in
                self?.popUpVIew.alpha = 1.0
            })
        } else {
            UIView.animate(withDuration: 0.6,
                           animations: { [weak self] in
                self?.popUpVIew.alpha = 0.0
            }) { [weak self] _ in
                self?.popUpVIew.isHidden = true
                self?.showMenu = false
            }
        }
    }
    
    private func hideMenu(){
        UIView.animate(withDuration: 0.6,
                       animations: { [weak self] in
            self?.popUpVIew.alpha = 0.0
        }) { [weak self] _ in
            self?.popUpVIew.isHidden = true
            self?.showMenu = false
        }
    }
    
    // MARK: - Selected Numbers
    private func selectedNumbers(btn : UIButton) {
        var selectedBtnCount = 0
        if btn == btn1 {
            lbl1.textColor = UIColor.white
            lbl2.textColor = Color.appColor
            lbl3.textColor = Color.appColor
            lbl4.textColor = Color.appColor
            view1.backgroundColor = Color.appColor
            view2.backgroundColor = UIColor.white
            view3.backgroundColor = UIColor.white
            view4.backgroundColor = UIColor.white
            view2.makeBorder(2, color: Color.appColor)
            view3.makeBorder(2, color: Color.appColor)
            view4.makeBorder(2, color: Color.appColor)
            selectedBtnCount = 1
        } else if btn == btn2 {
            lbl2.textColor = UIColor.white
            lbl1.textColor = Color.appColor
            lbl3.textColor = Color.appColor
            lbl4.textColor = Color.appColor
            view2.backgroundColor = Color.appColor
            view1.backgroundColor = UIColor.white
            view3.backgroundColor = UIColor.white
            view4.backgroundColor = UIColor.white
            view1.makeBorder(2, color: Color.appColor)
            view3.makeBorder(2, color: Color.appColor)
            view4.makeBorder(2, color: Color.appColor)
            selectedBtnCount = 2
        } else if btn == btn3 {
            lbl3.textColor = UIColor.white
            lbl2.textColor = Color.appColor
            lbl1.textColor = Color.appColor
            lbl4.textColor = Color.appColor
            view3.backgroundColor = Color.appColor
            view2.backgroundColor = UIColor.white
            view1.backgroundColor = UIColor.white
            view4.backgroundColor = UIColor.white
            view2.makeBorder(2, color: Color.appColor)
            view1.makeBorder(2, color: Color.appColor)
            view4.makeBorder(2, color: Color.appColor)
            selectedBtnCount = 3
        } else {
            lbl4.textColor = UIColor.white
            lbl2.textColor = Color.appColor
            lbl3.textColor = Color.appColor
            lbl1.textColor = Color.appColor
            view4.backgroundColor = Color.appColor
            view2.backgroundColor = UIColor.white
            view3.backgroundColor = UIColor.white
            view1.backgroundColor = UIColor.white
            view2.makeBorder(2, color: Color.appColor)
            view3.makeBorder(2, color: Color.appColor)
            view1.makeBorder(2, color: Color.appColor)
            selectedBtnCount = 4
        }
        let vc = SelectFolderVC.instantiate(fromAppStoryboard: .Main)
        vc.selectedBtnCount = selectedBtnCount
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
