//
//  LogoutModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import UIKit
import Foundation

// MARK: - LogoutModel
struct LogoutModel: Codable {
    let message: String?
    let status: Int?
    let data: LogoutData?
}

// MARK: - LogoutData
struct LogoutData: Codable {
}
