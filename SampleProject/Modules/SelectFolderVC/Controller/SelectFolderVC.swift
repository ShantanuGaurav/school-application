//
//  SelectFolderVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit
import Kingfisher

class SelectFolderVC: UIViewController {

    @IBOutlet weak var worldView: UIView!
    @IBOutlet weak var themeView: UIView!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var savedBtn: UIButton!
    @IBOutlet weak var themeBtn: UIButton!
    @IBOutlet weak var worldBtn: UIButton!
   
    var selectedBtnCount = 1
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        iniitalSetup()
    }
  
    // MARK: - Initial Setup
    private func iniitalSetup() {
        homeBtn.setTitle("", for: .normal)
        backBtn.setTitle("", for: .normal)
        themeBtn.setTitle("", for: .normal)
        worldBtn.setTitle("", for: .normal)
        savedBtn.makeBorder(2, color: Color.appColor)
        worldView.makeRoundCorner(10)
        worldView.makeBorder(0.5, color: Color.appColor)
        themeView.makeRoundCorner(10)
        themeView.makeBorder(0.5, color: Color.appColor)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        savedBtn.makeRoundCorner(savedBtn.frame.height/2)
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case homeBtn:
            self.navigationController?.popViewController(animated: true)
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        case themeBtn:
            let vc = SuperFolderVC.instantiate(fromAppStoryboard: .Main)
            vc.carriedDataObj.selectedPictureGroup = .Themes
            vc.carriedDataObj.selectedBtnCount = self.selectedBtnCount
            vc.carriedDataObj.collectionType = .Regular
            self.navigationController?.pushViewController(vc, animated: true)
        case worldBtn:
            let vc = SuperFolderVC.instantiate(fromAppStoryboard: .Main)
            vc.carriedDataObj.selectedPictureGroup = .FirstWords
            vc.carriedDataObj.selectedBtnCount = self.selectedBtnCount
            vc.carriedDataObj.collectionType = .Regular
            self.navigationController?.pushViewController(vc, animated: true)
        case savedBtn:
            let vc = SavedListVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
}
