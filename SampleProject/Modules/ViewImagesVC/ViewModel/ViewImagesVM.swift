//
//  ViewImagesVM.swift
//  SampleProject
//
//  Created by Shivam Garg on 11/04/22.
//

import Foundation


protocol ViewImagesProtocolDelegate : AnyObject {
    func parseImagesData( data : ImagesListModel, page: Int)
}

import PromiseKit

class ViewImagesVM {

    weak var delegate : ViewImagesProtocolDelegate?
    
    // MARK: - Saved List API
    func getImagesList(params: [String:Any], page:Int) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.getImagesList(params: params)
        }.done({ (model: ImagesListModel) in
            self.handleResponseImageList(model: model, page: page)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Saved List API
    func handleResponseImageList(model: ImagesListModel, page:Int) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.parseImagesData(data: model, page: page)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
    
}
