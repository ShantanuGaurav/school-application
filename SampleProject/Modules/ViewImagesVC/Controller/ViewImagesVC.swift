//
//  ViewImagesVC.swift
//  SampleProject
//
//  Created by Shivam Garg on 11/04/22.
//

import UIKit

class ViewImagesVC: UIViewController {

    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var imagesArray : [SavedListImage]?
    var paginationImagesArray : [SavedListImage]?
    var collectionName  = ""
    var pageNumber = 1
    var collectionId = -1
    var viewModel = ViewImagesVM()
    var objCollectionImageListModel : ImagesListModel?
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - View will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
   
    // MARK: - View did load
    private func initialSetup() {
        
        viewModel.delegate = self
        backBtn.setTitle("", for: .normal)
        homeBtn.setTitle("", for: .normal)
        collectionView.registerCell(with: SelectPictureCollCell.self)
        headerLbl.text = collectionName
        
        self.hitServiceToGetSavedList()
        
    }
    
    func hitServiceToGetSavedList(page : Int = 1){
        let params : [String : Any] = [
            "page": "\(page)",
            "collection_id": "\(collectionId)"
        ]
        viewModel.getImagesList(params: params, page: page)
    }

    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case homeBtn:
            self.navigationController?.popToViewController(ofClass: HomeVC.self)
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
}
