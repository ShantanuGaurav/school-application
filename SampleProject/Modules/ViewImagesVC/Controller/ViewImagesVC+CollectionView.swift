//
//  ViewImagesVC+CollectionView.swift
//  SampleProject
//
//  Created by Shivam Garg on 11/04/22.
//

import Foundation
import UIKit

// MARK: - Sub Folder VC Collection Delegate
extension ViewImagesVC : CollectionDelegate {
    
    // MARK: - Number of items in section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.objCollectionImageListModel?.data?.data?.count ?? 0 == 0 ? (collectionView.setEmptyMessage("No Data Available")) : (collectionView.restore())
        return self.objCollectionImageListModel?.data?.data?.count ?? 0
    }
    
    // MARK: - Cell for item at
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: SelectPictureCollCell.self, indexPath: indexPath)
        
        guard let data = self.objCollectionImageListModel?.data?.data?[indexPath.row] else {return UICollectionViewCell()}
        cell.populateCollectionImagesData(data : data)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row == (self.objCollectionImageListModel?.data?.data?.count ?? 0) - 1 {  //numberofitem count
            if self.pageNumber < (self.objCollectionImageListModel?.data?.lastPage ?? 1){
                self.hitServiceToGetSavedList(page: pageNumber + 1)
            }
        }
        
    }
    
    // MARK: - Size for item at
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 75) / 3.0 , height: ((collectionView.frame.width - 50) / 3.0) * 1.04)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
}
