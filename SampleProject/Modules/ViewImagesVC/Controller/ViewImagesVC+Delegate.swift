//
//  ViewImagesVC+Delegate.swift
//  SampleProject
//
//  Created by Shivam Garg on 11/04/22.
//

import Foundation
import UIKit

// MARK: - Saved List VC Protocol Delegate
extension ViewImagesVC : ViewImagesProtocolDelegate {
    func parseImagesData(data: ImagesListModel, page: Int) {
        if page == 1{
            self.objCollectionImageListModel = data
        }
        else{
            self.pageNumber += 1
            guard let data = data.data?.data else{
                return
            }
            self.objCollectionImageListModel?.data?.data! += data
        }
        
        self.collectionView.reloadData()
    }

}
