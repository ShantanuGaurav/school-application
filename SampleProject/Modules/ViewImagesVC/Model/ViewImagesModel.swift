//
//  ViewImagesModel.swift
//  SampleProject
//
//  Created by Shivam Garg on 11/04/22.
//

import Foundation

// MARK: - ImagesListModel
struct ImagesListModel: Codable {
    let status: Int?
    let message: String?
    var data: ImagesListDataClass?
}

// MARK: - DataClass
struct ImagesListDataClass: Codable {
    let currentPage: Int?
    var data: [ImagesListData]?
    let firstPageURL: String?
    let from, lastPage: Int?
    let lastPageURL: String?
    //let links: [Link]?
    let nextPageURL: String?
    let path: String?
    let perPage: Int?
    let prevPageURL: String?
    let to, total: Int?

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case data
        case firstPageURL = "first_page_url"
        case from
        case lastPage = "last_page"
        case lastPageURL = "last_page_url"
        //case links
        case nextPageURL = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageURL = "prev_page_url"
        case to, total
    }
}

// MARK: - Datum
struct ImagesListData: Codable {
    let id: Int?
    let categoryID: Int?
    let imagePath: String?
    let imageOrder: Int?
    let createdAt, updatedAt, isDeleted, imageName: String?
    let subfolderID: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case imagePath = "image_path"
        case imageOrder = "image_order"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isDeleted = "is_deleted"
        case imageName = "image_name"
        case subfolderID = "subfolder_id"
    }
}
