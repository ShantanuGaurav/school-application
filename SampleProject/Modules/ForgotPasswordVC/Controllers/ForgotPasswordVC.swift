//
//  ForgotPasswordVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var warningBtn: UIButton!
    @IBOutlet weak var emailTxt: UITextField!
   
    var viewModel = ForgotPasswordVM()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        viewModel.delegate = self
        backBtn.setTitle("", for: .normal)
        warningBtn.setTitle("", for: .normal)
        emailView.makeBorder(1, color: Color.appBorderColor)
        
        emailView.makeRoundCorner(10)
        mainView.makeRoundCorner(10)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        submitBtn.makeRoundCorner(submitBtn.frame.height/2)
    }
    
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        case submitBtn:
            validation()
        case warningBtn:
            let vc = InfoVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    
    // MARK: - Validation
    private func validation() {
        guard let emailAddress = emailTxt.text?.trim, !emailAddress.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.enterEmail)
            return
        }
        guard Common.instance.isValidEmail(testStr: emailTxt.text!) else{
            DisplayBanner.infoBanner(message:Constant.validEmailId)
            return
        }
        self.view.endEditing(true)
        let params : [String : Any] = [
            "email": emailAddress
        ]
        viewModel.forgotPassword(params: params)
    }
}
