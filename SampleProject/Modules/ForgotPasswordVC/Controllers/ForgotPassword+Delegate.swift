//
//  ForgotPassword+Delegate.swift
//  SampleProject
//
//  Created by Shivam Garg on 22/02/22.
//

import Foundation
import UIKit

extension ForgotPasswordVC : ForgotPasswordProtocolDelegate  {
    func passwordForgotSucess() {
        let vc = LinkPopupVC.instantiate(fromAppStoryboard: .Registration)
        vc.dissmissCallBack = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
}
