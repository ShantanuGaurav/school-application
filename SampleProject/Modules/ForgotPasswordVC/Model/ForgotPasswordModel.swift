//
//  ForgotPasswordModel.swift
//  SampleProject
//
//  Created by Shivam Garg on 22/02/22.
//

import Foundation

// MARK: - ChangePasswordModel
struct GenericModel: Codable {
    let message: String?
    let status: Int?
}
