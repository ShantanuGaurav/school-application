//
//  ForgotPasswordVM.swift
//  SampleProject
//
//  Created by Shivam Garg on 22/02/22.
//

import Foundation
import PromiseKit

protocol ForgotPasswordProtocolDelegate : AnyObject {
    func passwordForgotSucess()
}

class ForgotPasswordVM {
    
    weak var delegate : ForgotPasswordProtocolDelegate?
    
    // MARK: - Change Password API
    func forgotPassword(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.forgotPassword(params: params)
        }.done({ (model: ChangePasswordModel) in
            self.handleResponse(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Change Password API
    func handleResponse(model: ChangePasswordModel) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.passwordForgotSucess()
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
