//
//  ChangePasswordModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import UIKit
import Foundation

// MARK: - ChangePasswordModel
struct ChangePasswordModel: Codable {
    let message: String?
    let status: Int?
    let data: ChangePasswordData?
}

// MARK: - ChangePasswordData
struct ChangePasswordData: Codable {
}
