//
//  ChangePasswordVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import PromiseKit

protocol ChangePasswordProtocolDelegate : AnyObject {
    func passwordChangeSucessfully()
}

class ChangePasswordVM {
    
    weak var delegate : ChangePasswordProtocolDelegate?
    
    // MARK: - Change Password API
    func chnagePassword(params: [String:Any]) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.changePassword(params: params)
        }.done({ (model: ChangePasswordModel) in
            self.handleResponseChangePassowrd(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Change Password API
    func handleResponseChangePassowrd(model: ChangePasswordModel) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.passwordChangeSucessfully()
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
