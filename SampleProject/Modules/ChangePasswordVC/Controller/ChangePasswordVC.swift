//
//  ChangePasswordVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var currentView: UIView!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var newTxt: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var currentTxt: UITextField!
    @IBOutlet weak var confirmTxt: UITextField!
   
    var viewModel = ChangePasswordVM()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Initial Setup
    private func initialSetup() {
        viewModel.delegate = self
        backBtn.setTitle("", for: .normal)
        newView.makeBorder(1, color: Color.appBorderColor)
        currentView.makeBorder(1, color: Color.appBorderColor)
        confirmView.makeBorder(1, color: Color.appBorderColor)
        
        //
        mainView.makeRoundCorner(10)
        currentView.makeRoundCorner(10)
        newView.makeRoundCorner(10)
        confirmView.makeRoundCorner(10)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        submitBtn.makeRoundCorner(submitBtn.frame.height/2)
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case backBtn:
            self.navigationController?.popViewController(animated: true)
        case submitBtn:
            validation()
        default:
            break
        }
    }
    
    // MARK: - Validation
    private func validation() {
        guard let currentPassword = currentTxt.text, !currentPassword.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.currentPassword)
            return
        }
        if currentPassword.count < 6 || currentPassword.count > 15 {
            DisplayBanner.infoBanner(message:Constant.invalidCurrentPassword)
            return
        }
        guard let newPassword = newTxt.text, !newPassword.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.newPassword)
            return
        }
        if newPassword.count < 6 || newPassword.count > 15 {
            DisplayBanner.infoBanner(message:Constant.newPasswordLength)
            return
        }
        guard let confirm = confirmTxt.text, !confirm.isEmpty  else{
            DisplayBanner.infoBanner(message:Constant.confirmPassword)
            return
        }
        if confirm != newPassword {
            DisplayBanner.infoBanner(message:Constant.passwordMatch)
            return
        }
        let params : [String : Any] = [
            "password": currentPassword,
            "new_password": newPassword
        ]
        viewModel.chnagePassword(params: params)
    }
}
