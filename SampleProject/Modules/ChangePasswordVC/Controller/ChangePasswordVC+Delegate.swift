//
//  ChangePasswordVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

import UIKit
import Foundation

extension ChangePasswordVC : ChangePasswordProtocolDelegate  {
    
    // MARK: - Password Change Successfully
    func passwordChangeSucessfully() {
        self.newTxt.text = ""
        self.currentTxt.text = ""
        self.confirmTxt.text = ""
        DisplayBanner.infoBanner(message:Constant.passwordChange)
        self.navigationController?.popViewController(animated: true)
    }
}
