//
//  SubFolderVM.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 10/02/22.
//

import PromiseKit

protocol CategoriesImagesListProtocolDelegate : AnyObject {
    func parsingCategoriesImagesList(data : CategoriesImageListModel, page :Int, searchString : String)
}

class SubFolderVM {
    
    weak var delegate : CategoriesImagesListProtocolDelegate?
    
    // MARK: - Categories Image List API
    func categoriesImageList(params: [String:Any], page:Int, searchString : String) {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.catImageList(params: params)
        }.done({ (model: CategoriesImageListModel) in
            self.handleResponseCategoriesImageList(model: model, page: page, searchString: searchString)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Categories Image List API
    func handleResponseCategoriesImageList(model: CategoriesImageListModel, page:Int, searchString:String) {
        if model.status == 200  || model.status == 201 {
            self.delegate?.parsingCategoriesImagesList(data: model, page: page, searchString: searchString)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
}
