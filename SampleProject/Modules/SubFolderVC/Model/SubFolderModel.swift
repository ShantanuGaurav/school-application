//
//  SubFolderModel.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 10/02/22.
//

import Foundation

import Foundation

// MARK: - CategoriesImageListModel
struct CategoriesImageListModel: Codable {
    let status: Int?
    let message: String?
    var data: CategoriesImageListDataClass?
}

// MARK: - DataClass
struct CategoriesImageListDataClass: Codable {
    var categoryimage: Categoryimage?
}

// MARK: - Categoryimage
struct Categoryimage: Codable {
    let currentPage: Int?
    var data: [CategoriesImageList]?
    let firstPageURL: String?
    let from, lastPage: Int?
    let lastPageURL: String?
    //let links: [Link]?
    let nextPageURL, path: String?
    let perPage: Int?
    let prevPageURL: String?
    let to, total: Int?

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case data
        case firstPageURL = "first_page_url"
        case from
        case lastPage = "last_page"
        case lastPageURL = "last_page_url"
        //case links
        case nextPageURL = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageURL = "prev_page_url"
        case to, total
    }
}

// MARK: - Datum
struct CategoriesImageList: Codable {
    let id, categoryID: Int?
    let imagePath: String?
    let imageOrder: Int?
    let createdAt, updatedAt, isDeleted, imageName: String?
    let subfolderID: Int?
    let isSelected: String?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case imagePath = "image_path"
        case imageOrder = "image_order"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isDeleted = "is_deleted"
        case imageName = "image_name"
        case subfolderID = "subfolder_id"
        case isSelected = "is_selected"
    }
}
