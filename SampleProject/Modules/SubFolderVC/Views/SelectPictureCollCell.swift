//
//  SelectPictureCollCell.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 03/02/22.
//

import UIKit

class SelectPictureCollCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var imgVIew: UIImageView!
   
    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        selectBtn.setTitle("", for: .normal)
        mainView.makeRoundCorner(12)
        btnView.roundCornersFinal(corners: .bottomLeft, radius: 12)
        btnView.makeBorder(1.0, color: UIColor(hex: "#CCCCCC"))
        
        mainView.makeBorder(1.0, color: UIColor(hex: "#CCCCCC"))
    }
    
    // MARK: - Populate Data of Images
    func populateImagesData(data : CategoriesImageList) {
        btnView.isHidden = false
        nameLbl.text = data.imageName ?? ""
        imgVIew.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(data.imagePath ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
    }
    
    // MARK: - Populate Collection Images
    func populateCollectionImagesData(data : ImagesListData) {
        nameLbl.text = data.imageName ?? ""
        imgVIew.kf.setImage(with: URL(string: "\(ServerURL.imageBaseURL)\(data.imagePath ?? "")".removeSpaceFromUrl()), placeholder: UIImage(named: "placeHolder"))
        btnView.isHidden = true
    }
}
