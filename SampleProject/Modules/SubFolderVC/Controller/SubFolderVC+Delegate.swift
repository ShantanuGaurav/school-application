//
//  SubFolderVC+Delegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 10/02/22.
//

import UIKit
import Foundation

// MARK: - Sub Folder VC Protocol Delegate
extension SubFolderVC : CategoriesImagesListProtocolDelegate, SavedColletion {
    
    // MARK: - back to root
    func backToRoot(data: SavedListDatum?) {
        let collectionData:[String: SavedListDatum?] = ["collection": data]
        NotificationCenter.default.post(name: Notification.Name("SendUpdatedCollectionNotification"), object: collectionData)
        self.navigationController?.popToViewController(ofClass: SavedListVC.self)
    }
    
    // MARK: - Reload Data
    func reloadData() {
        
        //Remove few VC
        //self.navigationController?.removeViewController(SuperFolderVC.self)
        //self.navigationController?.removeViewController(CategorieVC.self)
        //self.navigationController?.removeViewController(SubFolderVC.self)
        
        //Add SavedListVC in stack
        let savedListVC = SavedListVC.instantiate(fromAppStoryboard: .Main)
        let homeVC = HomeVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.viewControllers = [homeVC, savedListVC, self]
        
        self.navigationController?.popToViewController(ofClass: SavedListVC.self)
    }

    // MARK: - Parsing Categories Images List Data
    func parsingCategoriesImagesList(data : CategoriesImageListModel, page:Int, searchString: String) {
        
        self.searchText = self.searchTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        if page == 1{
            self.objCategoriesImageListModel = data
        }
        else{
            self.pageNumber += 1
            guard let data = data.data?.categoryimage?.data else{
                return
            }
            self.objCategoriesImageListModel?.data?.categoryimage?.data! += data
        }
        
        self.objCategoriesImageListModel?.data?.categoryimage?.data?.forEach({
            self.allImagesArray.append(SelectedImageStruct(selectedImgID: $0.id ?? -1, selectedImgString:  $0.imagePath ?? ""))
        })
        
        selectAllCheck()
        
        self.collectionView.reloadData()
    }
}
