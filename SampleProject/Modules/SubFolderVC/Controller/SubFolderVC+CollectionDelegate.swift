//
//  SubFolderVC+CollectionDelegate.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 04/02/22.
//

import UIKit
import Foundation

// MARK: - Sub Folder VC Collection Delegate
extension SubFolderVC : CollectionDelegate {
    
    // MARK: - Number of items in section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.objCategoriesImageListModel?.data?.categoryimage?.data?.count ?? 0 == 0 ? (collectionView.setEmptyMessage("No Data Available")) : (collectionView.restore())
        return self.objCategoriesImageListModel?.data?.categoryimage?.data?.count ?? 0
    }
    
    // MARK: - Cell for item at
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: SelectPictureCollCell.self, indexPath: indexPath)
        
        guard let model = self.objCategoriesImageListModel?.data?.categoryimage?.data?[indexPath.row] else {return UICollectionViewCell()}
        cell.populateImagesData(data : model)
        
        if self.carriedDataObj.selectedImgID.map({$0.selectedImgID}).contains(model.id ?? -1) {
            cell.selectBtn.setImage(UIImage(named: "pic_select"), for: .normal)
        } else {
            cell.selectBtn.setImage(UIImage(named: "pic_unselect"), for: .normal)
        }
        
        cell.selectBtn.isUserInteractionEnabled = false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = self.objCategoriesImageListModel?.data?.categoryimage?.data?[indexPath.row] else {return}
        if let indx = self.carriedDataObj.selectedImgID.firstIndex(where: {$0.selectedImgID == data.id}){
            self.carriedDataObj.selectedImgID.remove(at: indx)
        } else {
            let imageStruct = SelectedImageStruct(selectedImgID: data.id ?? -1, selectedImgString: data.imagePath ?? "")
            self.carriedDataObj.selectedImgID.append(imageStruct)
        }
        print(self.carriedDataObj.selectedImgID)
        
        
        self.selectAllCheck()
        
        
        self.collectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        /*
        if indexPath.row == (self.objCategoriesImageListModel?.data?.categoryimage?.data?.count ?? 0) - 1 {  //numberofitem count
            if self.pageNumber < (self.objCategoriesImageListModel?.data?.categoryimage?.lastPage ?? 1){
                self.hitServiceCategoryImageList(page: pageNumber + 1, search: searchText)
            }
        }
        */
        
    }
    
    // MARK: - Size for item at
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 75) / 3.0 , height: ((collectionView.frame.width - 50) / 3.0) * 1.04)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
}
