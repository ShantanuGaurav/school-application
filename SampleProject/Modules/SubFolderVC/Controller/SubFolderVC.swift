//
//  SubFolderVC.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 24/01/22.
//

import UIKit

struct SelectedDataStruct{
    var selectedPictureGroup : PictureGroupEnum = .FirstWords
    var selectedBtnCount = 1
    
    var catID = -1
    var selectedImgID = [SelectedImageStruct]()
    var collectionType : CollectionType = .Regular
    
    var collectionID = -1
    var collectionName = ""
    
}

struct SelectedImageStruct{
    var selectedImgID = Int()
    var selectedImgString = ""
}

enum CollectionType{
    case Regular, Update
}

class SubFolderVC: UIViewController {
    
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var selectAllBtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
   
    var viewModel = SubFolderVM()
    var objCategoriesImageListModel : CategoriesImageListModel?
    var pageNumber = 1
    var searchText = ""
    var passSelectedPictureIdsArray : ((_ ids :[SelectedImageStruct])->())?
    var carriedDataObj = SelectedDataStruct()
    var allImagesArray = [SelectedImageStruct]()
    
    //Pull to Refresh
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                                    #selector(refreshPage(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.white
        return refreshControl
    }()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - View will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
   
    // MARK: - View did load
    private func initialSetup() {
        viewModel.delegate = self
        self.searchTxt.delegate = self
        
        backBtn.setTitle("", for: .normal)
        homeBtn.setTitle("", for: .normal)
        saveBtn.makeBorder(2, color: Color.appColor)
        selectAllBtn.makeBorder(1.0, color: UIColor(hex: "#A7C6ED"))
        selectAllBtn.setImage(UIImage(named: "selectAll_uncheck"), for: .normal)
        selectAllBtn.setImage(UIImage(named: "selectAll_check"), for: .selected)
        selectAllBtn.isSelected = false
        
        collectionView.registerCell(with: SelectPictureCollCell.self)
        collectionView.refreshControl = refreshControl
        searchTxt.setupRightImage(imageName: "Icon ionic-ios-search")
        self.searchView.makeBorder(1, color: Color.searchBorderColor)
        
        //MARK: Removed Pagination for select All functionality
        self.hitServiceCategoryImageList()
    }
    
    func hitServiceCategoryImageList(page : Int = 1, search : String = ""){
        var params = [String : Any]()
        if carriedDataObj.selectedPictureGroup == .FirstWords{
            params = [
                "category_id": self.carriedDataObj.catID, //1
                //"page": "\(page)",
                "search": "\(search)"
            ]
        }
        else{
            params = [
                "subfolder_id": self.carriedDataObj.catID, //1
                //"page": "\(page)",
                "search": "\(search)"
            ]
        }
        
        viewModel.categoriesImageList(params: params, page: page, searchString: search)
    }
    
    //Pull to Refresh
    @objc func refreshPage(_ refreshControl: UIRefreshControl) {
        //Hit Service
        self.pageNumber = 1
        self.searchTxt.text = ""
        self.searchText = ""
        self.hitServiceCategoryImageList()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        saveBtn.makeRoundCorner(self.saveBtn.height/2.0)
        startBtn.makeRoundCorner(self.startBtn.height/2.0)
        searchView.makeRoundCorner(self.searchView.height/2.0)
        selectAllBtn.makeRoundCorner(self.selectAllBtn.height/2.0)
    }
    
    // MARK: - Tap on buttons
    @IBAction func tapOnButtons(_ sender: UIButton) {
        switch sender {
        case homeBtn:
            self.navigationController?.popToViewController(ofClass: HomeVC.self)
        case backBtn:
            self.passSelectedPictureIdsArray?(self.carriedDataObj.selectedImgID)
            self.navigationController?.popViewController(animated: true)
        case selectAllBtn:
            
            if allImagesArray.count <= 0 { return }
            
            //MARK: Tap on Select All
            if !self.selectAllBtn.isSelected{
                
                self.allImagesArray.forEach({
                    let id = $0.selectedImgID
                    
                    //Add Images, which are not included already in Array
                    if !self.carriedDataObj.selectedImgID.contains(where: {$0.selectedImgID == id}){
                        self.carriedDataObj.selectedImgID.append(SelectedImageStruct(selectedImgID: $0.selectedImgID , selectedImgString:  $0.selectedImgString))
                    }
                })
                
                self.selectAllBtn.isSelected = true
                
            }
            else{
                //MARK: Tap on Deselect All
                
                //Remove all the images of this page from the list
                self.carriedDataObj.selectedImgID = self.carriedDataObj.selectedImgID.filter({ item in !self.allImagesArray.contains(where: { $0.selectedImgID == item.selectedImgID }) })
                
                self.selectAllBtn.isSelected = false
            }
            
            self.collectionView.reloadData()
            
        case startBtn:
            print("Start")
            let vc = StartResultVC.instantiate(fromAppStoryboard: .Main)
            vc.selectedBtnCount = self.carriedDataObj.selectedBtnCount
            vc.imagesArray = self.carriedDataObj.selectedImgID
            self.navigationController?.pushViewController(vc, animated: true)
        case saveBtn:
            validation()
        default:
            break
        }
    }
    
    func selectAllCheck(){
        var isSelectAll = true
        
        for i in self.allImagesArray{
            if !self.carriedDataObj.selectedImgID.contains(where: {$0.selectedImgID == i.selectedImgID}){
                isSelectAll = false
                break
            }
        }
        
        isSelectAll ? (self.selectAllBtn.isSelected = true) : (self.selectAllBtn.isSelected = false)
    }
    
    // MARK: - Validation
    private func validation() {
        if self.carriedDataObj.selectedImgID.count != 0 {
            let vc = SaveCollectionPopupVC.instantiate(fromAppStoryboard: .Main)
            vc.delegate = self
            vc.carriedDataObj = self.carriedDataObj
            self.present(vc, animated: true, completion: nil)
        } else {
            DisplayBanner.infoBanner(message:Constant.selectID)
            return
        }
    }
}
