//
//  Device.swift
//  PlinkedUser
//
//  Created by Vijay's Macbook on 05/09/21.
//

import UIKit

extension UIDevice {
    var hasNotch: Bool {
        let bottom = AppConstants.getKeyWindow().safeAreaInsets.bottom
        return bottom > 0
    }
}
