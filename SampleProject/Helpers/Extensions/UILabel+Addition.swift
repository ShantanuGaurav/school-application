//
//  UILabel+Addition.swift
//  Plinkd
//
//  Created by Vijay on 04/07/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import UIKit
import Foundation

class RegularLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontRegular(ofSize: size)
    }
}

class MediumLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontMedium(ofSize: size)
    }
}


class BoldLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontBold(ofSize: size)
    }
}

class SemiBoldLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontSemiBold(ofSize: size)
    }
}

extension UILabel{
    func style(fontName: String, fontSize: CGFloat, textColor: UIColor, backgroundColor: UIColor = .clear){
        font = UIFont.kAppRubikFont(ofSize: fontSize, fontName: fontName)
        self.textColor = textColor
        self.backgroundColor = backgroundColor
        
    }
    
//    func fontStyle(fontType: FontType, fontSize: FontSize, textColor: UIColor, textStyle: UIFont.TextStyle? = nil, isDynamic: Bool = true, backgroundColor: UIColor = .clear){
//        if isDynamic{
//            adjustsFontForContentSizeCategory = true
//            font = DynamicFont.font(type: fontType, size: fontSize, textStyle: textStyle)
//        }else{
//            font = StaticFont.font(type: fontType, size: fontSize)
//            
//        }
//        self.textColor = textColor
//        self.backgroundColor = backgroundColor
//        
//    }
}
