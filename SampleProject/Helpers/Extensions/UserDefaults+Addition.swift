//
//  UserDefaults+Addition.swift
//  Plinkd
//
//  Created by Vijay on 09/07/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import UIKit
import Foundation

/*
struct UserType {
    static let personal = "personal"
    static let business = "business"
}

@propertyWrapper
struct UserDefaultsBacked<Value : Codable> {
    let key: String
    let defaultValue: Value
    var storage: UserDefaults = .standard
    
    var wrappedValue: Value {
        get {
            // Read value from UserDefaults
            guard let data = storage.value(forKey: key) as? Data else {
                // Return defaultValue when no data in UserDefaults
                return defaultValue
            }
            // Convert data to the desire data type
            let value = try? JSONDecoder().decode(Value.self, from: data)
            return value ?? defaultValue
        }
        set {
            if let optional = newValue as? AnyOptional, optional.isNil {
                storage.removeObject(forKey: key)
            } else {
                // Convert newValue to data
                let data = try? JSONEncoder().encode(newValue)
                storage.setValue(data, forKey: key)
            }
        }
    }
}

extension UserDefaultsBacked where Value: ExpressibleByNilLiteral {
    init(key: String, storage: UserDefaults = .standard) {
        self.init(key: key, defaultValue: nil, storage: storage)
    }
}

struct AppData {
    @UserDefaultsBacked(key: CodingKeys.loggedInUsers.rawValue, defaultValue: [LoginUser()])
    static var loggedInUsers: [LoginVC]
    enum CodingKeys: String, CodingKey {
        case loggedInUsers
    }
}

extension UserDefaults {
    
    func setLoggedInUsers(user: LoginVC) {
        do {
            var existingUser = getLoggedInUsers()
            if !existingUser.contains(user) {
                existingUser.append(user)
            }
            let encoder = JSONEncoder()
            let data = try encoder.encode(existingUser)
            set(data, forKey: UserDefaultKeys.kLoggedInUsers)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func getLoggedInUsers() -> [LoginVC] {
        if let usersData = value(forKey: UserDefaultKeys.kLoggedInUsers) as? Data {
            do {
                let decoder = JSONDecoder()
                return try decoder.decode(Array.self, from: usersData) as [LoggedInUser]
            } catch let error {
                print(error.localizedDescription)
            }
        }
        return []
    }
    
    func deleteLoggedInUsers() {
        do {
            let existingUser: [LoggedInUser] = []
            let encoder = JSONEncoder()
            let data = try encoder.encode(existingUser)
            set(data, forKey: UserDefaultKeys.kLoggedInUsers)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func getCurrentAccount() {
        
    }
    
    func isUserLogin() -> Bool {
        return bool(forKey: UserDefaultKeys.kIsLoggedIn)
    }
    
    func isProfileCompleted() -> Bool {
        return bool(forKey: UserDefaultKeys.kIsProfileCompleted)
    }
    
    func getUserId() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kUserId))
    }
    
    func getAccessToken() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kAccessToken))
    }
    
    func setDeviceToken(deviceToken: String) {
        self.set(deviceToken, forKey: UserDefaultKeys.kDeviceToken)    
    }
    
    func getDeviceToken () -> String {
        return String.getString(string(forKey: UserDefaultKeys.kDeviceToken))
    }
    
    func getFacebookId() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kFacebookId))
    }
    
    func getFirstName() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kFirstName))
    }

    func getBusinessName() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kDob))
    }
    
    func getLastName() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kLastName))
    }

    func getReviews() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kReviews))
    }
        
    func getUserDescription() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kDescription))
    }

    func getUserEmail() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kLoggedInEmail))
    }
    
    func getUserProfileImage() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kProfileImage))
    }

    func getUsername() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kUsername))
    }
    
    func getUserPhoneNumber() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kPhoneNumber))
    }

    func getUserPassword() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kPassword))
    }
    
    func getUserGender() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kGender))
    }
        
    func getUserLatitude() -> Double {
        return double(forKey: UserDefaultKeys.kUserLatitude)
    }        
    
    func getUserLongitude() -> Double {
        return double(forKey: UserDefaultKeys.kUserLongitude)        
    }
 
    func countryCode() -> String {
        return string(forKey: UserDefaultKeys.kCountryCode) ?? "+1"
    }
    
    func getAccountType() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kAccountType))
    }
    
    func getDOB() -> String {
        return String.getString(string(forKey: UserDefaultKeys.kDob))
    }
    
    func isBusiness() -> Bool {
        if string(forKey: UserDefaultKeys.kUserType) == UserType.business {
            return true
        } else {
            return false
        }        
    }
    
    func getUserType() -> String {
        return string(forKey: UserDefaultKeys.kUserType) ?? UserType.personal
    }
    
    func isLocationShared() -> Bool {
        bool(forKey: UserDefaultKeys.kAllowedLocation)
    }
    
    func findFriendPopupShown() -> Bool {
        bool(forKey: UserDefaultKeys.kFriendPopShown)
    }
}

struct UserDefaultKeys {
    static let kDob = "dob"
    static let kGender = "gender"
    static let kUserId = "userId"
    static let kReviews = "reviews"
    static let kFriends = "friends"
    static let kAddress = "address"
    static let kFullname = "fullname"
    static let kUserType = "userType"
    static let kUsername = "username"
    static let kPassword = "password"
    static let kLastName = "lastName"
    static let kIsLoggedIn = "isLogin"
    static let kLoggedInEmail = "email"
    static let kFirstName = "firstName"
    static let kFacebookId = "kFacebookId"
    static let kAllowedLocation = "friends"
    static let kDescription = "description"
    static let kAccessToken = "accessToken"
    static let kDeviceToken = "deviceToken"
    static let kPhoneNumber = "phoneNumber"
    static let kCountryCode = "countryCode"
    static let kAccountType = "kAccountType"
    static let kProfileImage = "profileImage"
    static let kUserLatitude = "userLatitude"
    static let kCurrentUserId = "currentUserId" 
    static let kLoggedInUsers = "loggedInUsers"
    static let kUserLongitude = "userLongitude"
    static let kIsOtherAccount = "isOtherAccount"
    static let kAccessTokenType = "accessTokenType"
    static let kFriendPopShown = "feiendPopupShown"
    static let kIsProfileCompleted = "isProfileCompleted"

}
*/
