//
//  UIFont+Extension.swift
//  Boba-iOS
//
//  Created by Vijay's Macbook on 15/11/18.
//  Copyright © 2018 MobileCoderz. All rights reserved.
//

import UIKit
import Foundation

extension UIFont {
    
    static let widthFactor: CGFloat = (kScreenWidth / 320) >= 1.2 ? 1.0 : 0.9
    
    class func printFontFamily() {
        for family in UIFont.familyNames {
            print("Family \(family)")
            for name in UIFont.fontNames(forFamilyName: family) {
                print("Members----\(name)")
            }
        }
    }
    
    static func kAppDefaultFontBold(ofSize size: CGFloat = 16) -> UIFont {
        return UIFont(name: "Poppins-Bold", size: size * widthFactor)!
    }
    
    static func kAppDefaultFontRegular(ofSize size: CGFloat = 16) -> UIFont {
        return UIFont(name: "Poppins-Regular", size: size * widthFactor)!
    }
        
    static func kAppDefaultFontSemiBold(ofSize size: CGFloat = 16) -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: size * widthFactor)!
    }
    
    static func kAppDefaultFontMedium(ofSize size: CGFloat = 16) -> UIFont {
        return UIFont(name: "Poppins-Medium", size: size * widthFactor)!
    }
    
    
    struct FontName {
        static let helveticaRegular = "Helvetica-Regular"
        static let helveticaNeueArabic = "HelveticaNeueLTArabic-Roman"
        
    }
}
