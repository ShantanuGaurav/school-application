//
//  Integer+Addition.swift
//  Plinkd
//
//  Created by Vijay Singh Raghav on 26/08/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import Foundation

extension Int {
    static func getInt(_ value: Any?) -> Int {
        guard let intValue = value as? Int else {
            guard let doubleValue = value as? Double else {
                let strInt = String.getString(value)
                guard let intValueOfString = Int(strInt) else {
                    return Int(Double.getDouble(strInt))
                }
                return intValueOfString
            }
            return Int(doubleValue)
        }
        return intValue
    }
}

extension Array {
    static func jsonObject(from object: Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
}
extension Int{
    func toString() -> String{
        return String(self)
    }
}
