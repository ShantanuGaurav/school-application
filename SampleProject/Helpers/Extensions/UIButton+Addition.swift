//
//  UIButton+Addition.swift
//  Plinkd
//
//  Created by Vijay on 04/07/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import UIKit
import Foundation

class RegularButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitle(titleLabel?.text?.localized(), for: .normal)
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        titleLabel?.font = UIFont.kAppDefaultFontRegular(ofSize: size)
    }
}

class MediumButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitle(titleLabel?.text?.localized(), for: .normal)
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        titleLabel?.font = UIFont.kAppDefaultFontMedium(ofSize: size)
    }
}

class BoldButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitle(titleLabel?.text?.localized(), for: .normal)
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        titleLabel?.font = UIFont.kAppDefaultFontBold(ofSize: size)
    }
}

class SemiBoldButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitle(titleLabel?.text?.localized(), for: .normal)
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        titleLabel?.font = UIFont.kAppDefaultFontSemiBold(ofSize: size)
    }
}
extension UIButton{
    func style(fontName: String, fontSize: CGFloat, textColor: UIColor,backgroundColor: UIColor = .clear){
        titleLabel?.font = UIFont.kAppRubikFont(ofSize: fontSize, fontName: fontName)
        setTitleColor(textColor, for: .normal)
        setTitleColor(textColor, for: .selected)
        self.backgroundColor = backgroundColor
        
    }
    
//    func fontStyle(fontType: FontType, fontSize: FontSize, textColor: UIColor, textStyle: UIFont.TextStyle? = nil, isDynamic: Bool = true, backgroundColor: UIColor = .clear){
//        if isDynamic{
//            titleLabel?.adjustsFontForContentSizeCategory = true
//            titleLabel?.font = DynamicFont.font(type: fontType, size: fontSize, textStyle: textStyle)
//        }else{
//            titleLabel?.font = StaticFont.font(type: fontType, size: fontSize)
//            
//        }
//        setTitleColor(textColor, for: .normal)
//        setTitleColor(textColor, for: .selected)
//
//        self.backgroundColor = backgroundColor
//        
//    }
}
extension UIFont {
    
    static func kAppRubikFont(ofSize size: CGFloat = 16 , fontName : String) -> UIFont {
        let widthFactor: CGFloat = kScreenWidth / 428
        return UIFont(name: fontName, size: size * widthFactor) ?? UIFont.systemFont(ofSize: 10)
    }
}
