//
//  Double+Addition.swift
//  Plinkd
//
//  Created by Vijay on 15/07/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import CoreGraphics

extension Double {
    static func getDouble(_ value: Any?) -> Double {
        guard let doubleValue = value as? Double else {
            let strDouble = "\(value ?? 0)"
            guard let doubleValueOfString = Double(strDouble) else {
                return 0.0
            }
            return doubleValueOfString
        }
        return doubleValue
    }
}

extension CGFloat {
    func toRadians() -> CGFloat {
        return self * CGFloat(Double.pi) / 180.0
    }
}

