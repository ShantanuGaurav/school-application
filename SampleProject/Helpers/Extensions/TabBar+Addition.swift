//
//  TabBar+Addition.swift
//  Plinkd
//
//  Created by Vijay's Macbook on 13/05/20.
//  Copyright © 2020 Vijay. All rights reserved.
//

import UIKit
import Foundation

extension UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        if kScreenHeight > 736 {
             sizeThatFits.height = kScreenHeight * 0.109
        } else {
            sizeThatFits.height = kScreenHeight * 0.08
        }
        return sizeThatFits
    }
}
