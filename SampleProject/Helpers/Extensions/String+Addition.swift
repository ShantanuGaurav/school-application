//
//  String+Addition.swift
//  Plinkd
//
//  Created by Vijay on 09/07/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import UIKit
import Foundation

extension String {    
    static func getString(_ message: Any?) -> String {
        if let string = message {
            guard let strMessage = string as? String else {
                guard let doubleValue = string as? Double else {
                    guard let intValue = string as? Int else {
                        guard string is Float else {
                            guard let int64Value = string as? Int64 else {
                                guard let int32Value = string as? Int32 else {
                                    guard let int16Value = string as? Int32 else {
                                        guard ((string as? CGFloat) != nil) else {
                                            return ""
                                        }
                                        return "\(string)"
                                    }
                                    return String(int16Value)
                                }
                                return String(int32Value)
                            }
                            return String(int64Value)
                        }
                        return String(format: "%.2f", string as! Float)
                    }
                    return String(intValue)
                }
                return String(format: "%.2f", doubleValue)
            }
            return strMessage
        }
        return ""
    }
    
    static func getJsonString(object: Any) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: object, options: [])            
            return String(data: jsonData, encoding: .utf8)
        } catch let error {
            Console.log(error.localizedDescription)
        }
        return nil
    }
    
    static func getJsonData(object: Any) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: object)
        } catch let error {
            Console.log(error.localizedDescription)
        }
        return nil
        
    }
    
    func isPasswordValid() -> Bool {        
        let passwordRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    func isAlphanumeric() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil && self != ""
    }
    
    func isValidName() -> Bool {
        let nameRegEx = "[A-Za-z ]+"        
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
    }
    
    func isValidEmail() -> Bool { //  "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isContactNumberValid() -> Bool {
        if(self.count >= 10 && self.count <= 16) {
            return true
        }
        else {
            return false
        }
    }
        
    func isAlphabets() -> Bool {
        let regex = try! NSRegularExpression(pattern: "[a-zA-Z\\s]+", options: [])
        let range = regex.rangeOfFirstMatch(in: self, options: [], range: NSRange(location: 0, length: count))
        return range.length == count
    }
    
    func isLengthValid(minLength: Int , maxLength: Int) -> Bool {
        return self.count >= minLength && self.count <= maxLength
    }

    func isValidPassword()-> Bool {
           let regex = try! NSRegularExpression(pattern: "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}", options: .caseInsensitive)
           return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
       
       }
    
    var capitalizedFirst: String {
        guard !isEmpty else { return self }
        var result = self
        let substr1 = String(self[startIndex]).uppercased()
        result.replaceSubrange(...startIndex, with: substr1)
        return result
    }

    
    public func validatePassword(password: String) -> Bool {
        let passRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        let trimmedString = password.trimmingCharacters(in: .whitespaces)
        let validatePassord = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isvalidatePass = validatePassord.evaluate(with: trimmedString)
        return isvalidatePass
    }    
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    var byRemovingLeadingTrailingWhiteSpaces: String {        
        let spaceSet = CharacterSet.whitespaces
        return self.trimmingCharacters(in: spaceSet)
    }

    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
    
    func underLinedString(color: UIColor = Color.appColor) -> NSAttributedString {
        return NSAttributedString(string: self, attributes:
                                    [.underlineStyle: NSUnderlineStyle.single.rawValue,
                                     NSAttributedString.Key.underlineColor: color])
    }
    
    func sendEncodedString() -> String {
        if let data = data(using: String.Encoding.nonLossyASCII) {
            if let encodedString = String(data: data, encoding: String.Encoding.utf8) {
                return encodedString
            }
        }
        return self
    }
    
    func showEncodedString() -> String {
        if let data  = self.data(using: String.Encoding.utf8){
            if let encodedString = String(data: data, encoding: String.Encoding.nonLossyASCII) {
                return encodedString
            }
        }
        return self
    }
    
    static func getAttributedText(firstString: String, middleString: String ,lastString: String, size: CGFloat) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: firstString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontBold(ofSize: size), NSAttributedString.Key.foregroundColor: UIColor.black]))
        attributedString.append(NSAttributedString(string: middleString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontRegular(ofSize: size - 1), NSAttributedString.Key.foregroundColor: UIColor.black]))
        attributedString.append(NSAttributedString(string: lastString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontBold(ofSize: size), NSAttributedString.Key.foregroundColor: UIColor.black]))
        return attributedString
    }
    
    func convertToDate(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        let localDate = formatter.date(from: self)
        return localDate ?? Date()
    }
    
    func toDateStringFromUTC( inputDateFormat : String,  ouputDateFormat: String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputDateFormat
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: self) // create   date from string
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = ouputDateFormat
        dateFormatter.timeZone = NSTimeZone.local
        if date != nil {
            return dateFormatter.string(from: date!)
        }
        else{
            return ""
        }
        
    }
    
    static func getAttributedTextOfFiveLength(firstString: String, secondString: String, thirdString: String, fourthString: String, fifthString: String, size: CGFloat) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: firstString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontBold(ofSize: size), NSAttributedString.Key.foregroundColor: UIColor.black]))
        attributedString.append(NSAttributedString(string: secondString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontRegular(ofSize: size - 1), NSAttributedString.Key.foregroundColor: UIColor.black]))
        attributedString.append(NSAttributedString(string: thirdString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontBold(ofSize: size - 1), NSAttributedString.Key.foregroundColor: UIColor.black]))
        attributedString.append(NSAttributedString(string: fourthString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontRegular(ofSize: size), NSAttributedString.Key.foregroundColor: UIColor.black]))
        attributedString.append(NSAttributedString(string: fifthString, attributes: [NSAttributedString.Key.font: UIFont.kAppDefaultFontBold(ofSize: size), NSAttributedString.Key.foregroundColor: UIColor.black]))
        return attributedString
    }
    
    static func join(strings: [String?]?, separator: String) -> String {
        guard let strings = strings else{return ""}
        return strings.compactMap { $0 }.joined(separator: separator)
    }
    var isNumeric: Bool {
        guard !isEmpty else { return false }
        let containsNonNumbers = contains { !$0.isNumber }
        return !containsNonNumbers
    }
    
    func removeSpaceFromUrl() -> String{
        self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
}

//struct Console {
//    static func log(_ message: Any?) {
//        #if DEBUG
//        print(message ?? "Either Value is nil or can't Invalid.")
//        #endif
//    }
//}

extension String {
    func getViewController() -> UIViewController? {
        if let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String {
            print("CFBundleName - \(appName)")
            if let viewControllerType = NSClassFromString("\(appName).\(self)") as? UIViewController.Type {
                return viewControllerType.init()
            }
        }
        return nil
    }
    
    var trim:String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var isBlank:Bool {
        return self.trim.isEmpty
    }
}

extension StringProtocol {
    var firstUppercased: String { return prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { return prefix(1).capitalized + dropFirst() }
}
