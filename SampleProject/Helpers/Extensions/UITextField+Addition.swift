//
//  UITextField+Addition.swift
//  Plinkd
//
//  Created by Vijay on 15/07/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import UIKit
import Foundation

class RegularField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontRegular(ofSize: size)
    }
}

class MediumField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontMedium(ofSize: size)
    }
}

class BoldField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontBold(ofSize: size)
    }
}


class SemiBoldField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = text?.localized()
        let size: CGFloat = CGFloat(tag == 0 ? 16 : tag)
        font = UIFont.kAppDefaultFontSemiBold(ofSize: size)
    }
}

extension UITextField {
    
    func setAttributedPlaceholder(placeholderText: String, font: UIFont, color: UIColor) {
        attributedPlaceholder = NSAttributedString(string: placeholderText,
                                                   attributes: [NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: font])
    }
    
    func applyLeftPadding(padding: Int) {
        let paddingView = UIView.init(frame: CGRect(x: 0,y:0,width: padding,height: Int(self.frame.size.height)))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setLeftImage(imageName: String, width: CGFloat = 25) {
        let containerView = UIView()
        containerView.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        let leftImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        leftImage.clipsToBounds = true
        containerView.addSubview(leftImage)
        leftImage.image = UIImage(named: imageName)
        leftImage.contentMode = .center
        leftImage.center = containerView.center
        leftViewMode = .always
        leftView = containerView
    }
    
    func setRightImage(imageName: String) {
        let containerView = UIView()
        containerView.frame = CGRect(x: 0, y: 0, width: frame.height, height: frame.height)
        let rightImage = UIImageView()
        rightImage.image = UIImage(named: imageName)
        rightImage.contentMode = .center
        containerView.addSubview(rightImage)
        rightImage.center = containerView.center
        rightViewMode = .always
        rightView = containerView
    }
    
    @IBInspectable var doneAccessory: Bool {
        get {
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone {
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 45))
        doneToolbar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        resignFirstResponder()
    }
}
extension UITextView{
    func style(fontName: String, fontSize: CGFloat, textColor: UIColor, backgroundColor: UIColor = .clear, placeholderColor: UIColor){
        font = UIFont.kAppRubikFont(ofSize: fontSize, fontName: fontName)
        self.textColor = textColor
        self.backgroundColor = backgroundColor
    }
}

extension UITextField {
    
    //MARK:- Set Image on the right of text fields
    
    func setupRightImage(imageName:String){
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 16, height: 18))
        imageView.image = UIImage(named: imageName)
        let imageContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 55, height: 40))
        imageContainerView.addSubview(imageView)
        rightView = imageContainerView
        rightViewMode = .always
        self.tintColor = .lightGray
    }
    
    //MARK:- Set Image on left of text fields
    
    func setupLeftImage(imageName:String){
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 18))
        imageView.image = UIImage(named: imageName)
        let imageContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 55, height: 40))
        imageContainerView.addSubview(imageView)
        leftView = imageContainerView
        leftViewMode = .always
        self.tintColor = .lightGray
    }
    
    func paddingView() {
        let paddingViews = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        self.leftViewMode = .always
        self.leftView = paddingViews
    }
}

class CustomTextField: UITextField{
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    let padding = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 40)
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

extension UITextField {
    fileprivate func setPasswordToggleImage(_ button: UIButton) {
        if(isSecureTextEntry){
            button.setImage(UIImage(named: "eye_off"), for: .normal)
        }else{
            button.setImage(UIImage(named: "eye_off"), for: .normal)
        }
    }
}


extension UITextField{
    func style(fontName: String, fontSize: CGFloat, textColor: UIColor, backgroundColor: UIColor = .clear, placeholderColor: UIColor){
        font = UIFont.kAppRubikFont(ofSize: fontSize, fontName: fontName)
        self.textColor = textColor
        self.backgroundColor = backgroundColor
        if let placeholder = placeholder{
        setAttributedPlaceholder(placeHolder: placeholder, color: placeholderColor, font: font)
        }
    }
    func setAttributedPlaceholder(placeHolder: String, color: UIColor , font: UIFont?) {
        if let font = font{
            let attributesDictionary = [NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: font]
            self.attributedPlaceholder = NSAttributedString(string: placeHolder, attributes: attributesDictionary)
        }else{
            let attributesDictionary = [NSAttributedString.Key.foregroundColor: color]
            self.attributedPlaceholder = NSAttributedString(string: placeHolder, attributes: attributesDictionary)
        }
    }
//    
//    func fontStyle(fontType: FontType, fontSize: FontSize, textColor: UIColor, textStyle: UIFont.TextStyle? = nil, isDynamic: Bool = true, backgroundColor: UIColor = .clear, placeholderColor: UIColor){
//        if isDynamic{
//            adjustsFontForContentSizeCategory = true
//            font = DynamicFont.font(type: fontType, size: fontSize, textStyle: textStyle)
//        }else{
//            font = StaticFont.font(type: fontType, size: fontSize)
//            
//        }
//        self.textColor = textColor
//        self.backgroundColor = backgroundColor
//        if let placeholder = placeholder{
//        setAttributedPlaceholder(placeHolder: placeholder, color: placeholderColor, font: font)
//        }
//        
//    }
    
    
}
