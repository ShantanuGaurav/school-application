//
//  UIApplication+Additions.swift
//  Boba-iOS
//
//  Created by VIJAY RAGHAV on 14/11/19.
//  Copyright © 2019 VIJAY RAGHAV. All rights reserved.
//

import UIKit
import Foundation

extension UIApplication {
    var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 38
            if let statusBar = kKeyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                guard let statusBarFrame = kKeyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
                let statusBarView = UIView(frame: statusBarFrame)
                statusBarView.tag = tag
                kKeyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        } else {
            return nil
        }
    }
}
