//
//  UIColor+Addition.swift
//  Plinkd
//
//  Created by Vijay on 28/06/19.
//  Copyright © 2019 Vijay. All rights reserved.
//
 
import UIKit
import Foundation

extension UIColor {
    
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var hexFormatted = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }
        
        assert(hexFormatted.count == 6, "Invalid hex code used.")
        
        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)
        
        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
    
    func customGreenColor() -> UIColor {
        return UIColor.init(hex: "7CC08A")
    }
    
    func customRedColor() -> UIColor {
        return UIColor.init(hex: "EE5656")
    }
}
struct Color {
    static let appColor = UIColor(hexFromString: "#0E6FB0")
    static let appBorderColor = UIColor(hexFromString: "#DEE1E5")
    static let searchBorderColor = UIColor(hexFromString: "#C1CCD3")
}

extension UIColor {
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
           var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
           var rgbValue:UInt64 = 10066329 //color #999999 if string has wrong format
           
           if (cString.hasPrefix("#")) {
               cString.remove(at: cString.startIndex)
           }
           
           let length = cString.count
           
           if length == 6 || length == 8 {
               Scanner(string: cString).scanHexInt64(&rgbValue)
           }
           
           if length == 8 {
               self.init( // #ARGB
                   red: CGFloat((rgbValue & 0x00FF0000) >> 16) / 255.0,
                   green: CGFloat((rgbValue & 0x0000FF00) >> 8) / 255.0,
                   blue: CGFloat(rgbValue & 0x000000FF) / 255.0,
                   alpha: CGFloat((rgbValue & 0xFF000000) >> 24) / 255.0
               )
               return
           }
           
           self.init( // RGB
               red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
               green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
               blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
               alpha: alpha
           )
       }
}
