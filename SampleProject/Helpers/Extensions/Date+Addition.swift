//
//  Date+Addition.swift
//  Plinkd
//
//  Created by Vijay Singh Raghav on 28/08/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import Foundation

extension Date {
    
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
        
    static func getTimeIntervalComponent(startDate: Date, endDate: Date) -> (Int, Int, Int) {
        let hour = Calendar.current.dateComponents([.hour], from: startDate, to: endDate).hour ?? 0
        let minute = Calendar.current.dateComponents([.minute], from: startDate, to: endDate).minute ?? 0
        let second = Calendar.current.dateComponents([.second], from: startDate, to: endDate).second ?? 0
        return (hour, minute, second)
    }
    
    static func getDateFromTimestamp(timeStamp:Double, formatter:DateFormatter) -> String {
        let date = Date(timeIntervalSince1970: timeStamp/1000)
        return formatter.string(from: date)
    }
    
    static func getDateFromString(dateString: String, formatter: DateFormatter) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = formatter.date(from: dateString)
        return dateFormatter.string(from: date ?? Date())
    }

    static func getFormattedStartTime(dateObject: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, hh:mm a"
        return dateFormatter.string(from: dateObject)
    }
    
    static func getDateStringFromDate(dateObject: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: dateObject)
    }

    static func getDateStringFromDateForDisplay(dateObject: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let weekDay = dateFormatter.weekdaySymbols[Calendar.current.component(.weekday, from: dateObject) - 1]
        return dateFormatter.string(from: dateObject) + ", \(weekDay)"
    }
    
    static func getTimeStringFromDate(dateObject: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: dateObject)
    }
    
    static func getEventTime(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: dateString)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh: mm a"
        return formatter.string(from: date ?? Date())
    }
            
    static func getEventDate(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter.string(from: date ?? Date())
    }
        
    static func getTimeFromTimeStamp(timeStamp: Int64) -> String {
        let unixtime = Double(timeStamp)
        let date = Date(timeIntervalSince1970: unixtime/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale  = Locale.current
        dateFormatter.timeZone = TimeZone.current
        let dateStr = dateFormatter.string(from: date)
        return dateStr
    }
    
    var timeStamp: Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
    static func getTimeDifference(timeStamp: Double) -> String {
        let startDate = Date(timeIntervalSince1970: timeStamp/1000)
        let endDate = Date()
        let indian = Calendar(identifier: .indian)
        let components =  indian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate , to: endDate)
        let years = components.year
        let months = components.month
        let days = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        if years == 0 {
            if months == 0 {
                if days == 0 {
                    if hour == 0 {
                        if minute == 0 {
                           // return "\(String.getString(second)) sec ago"
                            return "\(String.getString(second))" + (second == 1 ? " sec ago" : "sec ago")
                        } else {
                            //return "\(String.getString(minute)) min ago"
                            return "\(String.getString(minute))" + (minute == 1 ? " min ago" : "min ago")
                        }
                    } else {
                        return "\(String.getString(hour))h ago"
                    }
                } else {
                    return "\(String.getString(days))" + (days == 1 ? " d ago" : " d ago")
                }
            } else {
                return "\(String.getString(months))" + (months == 1 ? " mon ago" : " mon ago")
            }
        } else {
            return "\(String.getString(years))" + (years == 1 ? " y ago" : " yrs ago")
        }
    }    
}
