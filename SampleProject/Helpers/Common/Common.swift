//
//  Common.swift
//
//

import UIKit

class Common: NSObject {
    static let instance = Common()
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func setCornersOnlyTop(_ view: UIView, color: UIColor, borderWidth: CGFloat, value: CGFloat) {
        view.layer.cornerRadius = value
        view.clipsToBounds = true
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color.cgColor
    }
    
    func setCornersOnlyBottom(_ view: UIView, color: UIColor, borderWidth: CGFloat, value: CGFloat) {
        view.layer.cornerRadius = value
        view.clipsToBounds = true
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color.cgColor
    }
    
    func setCornerRadiusFor(_ view: UIView, value: CGFloat) {
        view.layer.cornerRadius = value
        view.layer.masksToBounds = true
    }
    
    func setCustomCornerRadiusAndBorderWidthFor(_ view: UIView, color: UIColor, borderWidth: CGFloat, value: CGFloat) {
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color.cgColor
        view.layer.cornerRadius = value
        view.layer.masksToBounds = true
    }
    
    func setCornerRadiusAndBorderWidthFor(_ view: UIView, color: UIColor, borderWidth: CGFloat) {
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color.cgColor
        view.layer.cornerRadius = (view.frame.size.height - 5)/2
        view.layer.masksToBounds = true
    }
    
    func setThickShadowFor(_ view: UIView, hexColor: String) {
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor(hex: hexColor).cgColor
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 8
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = .zero
        
    }
    
    // SET GRADIENT BACKGROUND COLOR
    func setGradientBackground(gradientColor: [CGColor], mainView: UIView, subView: UIView) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColor
        gradientLayer.locations = [0, 0, 0.7]
        gradientLayer.frame = mainView.bounds
        subView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    // SET GRADIENT BACKGROUND COLOR FOR TWO
    func setGradientBackgroundForTwo(gradientColor: [CGColor], mainView: UIView, subView: UIView) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColor
        gradientLayer.locations = [0, 0.7]
        gradientLayer.frame = mainView.bounds
        subView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func convertDateFormatIntoString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd MMM yyyy"
        let dateString = formatter.string(from: yourDate!)
        print(dateString)
        return dateString
    }
    
    func convertStringToDateFormat(dateStr: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatter.date(from: dateStr) else {
            return Date()
        }
        print(date)
        return date
    }
    
    //This is my padding function.
    func textFieldLeftPadding(textFieldName: UITextField) {
        textFieldName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textFieldName.frame.height))
        textFieldName.leftViewMode = .always//For left side padding
        textFieldName.rightViewMode = .always//For right side padding
    }
    
    // RETURN VIEWCONTROLLER WITH IDENTIFIER
    func getViewController(identifier: String) -> UIViewController {
        let viewController = UIStoryboard(name: "Registration", bundle: nil).instantiateViewController(withIdentifier: identifier)
        return viewController
    }
}

// COLOR
extension UIColor {
    convenience init (hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

// DOWNLOADING THE IMAGE
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .redraw) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .redraw) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}


struct NotificationName {
    static let changeIndexTap = NSNotification.Name("ChangeTab")
    static let backIndexTap = NSNotification.Name("BackToFirstTab")
}
