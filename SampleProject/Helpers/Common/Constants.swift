//
//  Constants.swift
//
//

import UIKit
import Foundation

let kUserDefaults = UserDefaults.standard
let kScreenWidth = UIScreen.main.bounds.width
let kScreenHeight = UIScreen.main.bounds.height
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let CWindow = CSharedApplication.keyWindow

@available(iOS 13.0, *)
let kKeyWindow = UIApplication.shared.connectedScenes
    .filter({$0.activationState == .foregroundActive})
    .map({$0 as? UIWindowScene})
    .compactMap({$0})
    .first?.windows
    .filter({$0.isKeyWindow}).first

@objcMembers
@objc class AppConstants: NSObject {
    private override init() {
        
    }
    
    @objc class func getKeyWindow() -> UIWindow {
        if #available(iOS 13.0, *) {
            return kKeyWindow ?? UIWindow()
        } else {
            return UIApplication.shared.keyWindow ?? UIWindow()
        }
    }
    
    @objc class func getStatusBarHeight() -> CGFloat {
        if #available(iOS 13.0, *) {
            return getKeyWindow().windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            return UIApplication.shared.statusBarFrame.height
        }
    }
}

struct Constant {
    
    // MARK: - Common
    static let ok = "OK"
    static let yes = "Yes"
    static let cancel = "Cancel"
    static let appName = "SchoolApplication"
    static let underDevelopment = "Under Development"
    
    // MARK: - Settings Array
    
    static let settingsArr = ["Notifications","Edit Profile","Payment Information","About Us"]
    
    // MARK: - Placeholder
    static let fullName = "Full name"
    static let email = "Email address"
    static let enterPassword = "Enter password"
    static let reEnterPassword = "Confirm password"
    
    // MARK: - PopUp Messages
    static let enterName = "Enter full name"
    static let nameMaxChar = "Name should be of maximum 40 characters"
    static let enterEmail = "Enter email address"
    static let logoutMsg = "Are you sure you want to logout?"
    static let validEmailId = "Enter valid email id"
    static let password = "Enter the password"
    static let passwordLength = "Password range should be from 6 to 15 characters"
    static let invalidPassword = "Password is invalid"
    
    static let invalidCurrentPassword = "Current password is invalid"
    static let newPasswordLength = "New Password range should be from 6 to 15 characters"
    
    static let currentPassword = "Enter the current password"
    static let newPassword = "Enter the new password"
    static let confirmPassword = "Enter the confirm password"
    static let passwordMatch = "Password does not match"
    static let passwordChange = "Password changed successfully"
    static let title = "Enter title to save list"
    static let selectID = "Select atleast one image to save list"
    static let deleteFromSavedList = "Are you sure you want to delete this list?"
}

struct AppKeys {
    static let token = "token"
    static let userData = "userData"
    static let purchasedFolders = "purchasedFolders"
}

let CSharedApplication = UIApplication.shared
