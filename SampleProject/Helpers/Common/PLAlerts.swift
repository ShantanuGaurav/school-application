//
//  PLAlerts.swift
//  PlinkedUser
//
//  Created by APPLE on 21/05/21.
//

import UIKit

enum NotificationColor: Int {
    case warn = 0, error, success
}

class PLAlerts: NSObject {
          
    class func showMessageWithDodoMessageBar(message:String, messageType: NotificationColor) {
       /* DispatchQueue.main.async
            {
                CNShared.getWindow().dodo.style.bar.hideAfterDelaySeconds = 3
                CNShared.getWindow().dodo.style.bar.hideOnTap = true
                CNShared.getWindow().dodo.style.label.shadowColor = DodoColor.fromHexString("#00000050")
                CNShared.getWindow().dodo.style.label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)
                let currentShowAnimation: DodoAnimations = .fade
                CNShared.getWindow().dodo.style.bar.animationShow = currentShowAnimation.show
                CNShared.getWindow().dodo.style.bar.animationHide = currentShowAnimation.hide
                
                var preset = DodoPresets.success
                switch messageType
                {
                case .success:
                    preset = DodoPresets.success
                case .warn:
                    preset = DodoPresets.warning
                case .error:
                    preset = DodoPresets.error
                }
                CNShared.getWindow().dodo.preset = preset
                CNShared.getWindow().dodo.show(message)
        }*/
    }

    class func showNetworkNotReachableMessage(messageType: NotificationColor) {
        PLAlerts.showMessageWithDodoMessageBar(message: "Please check your internet connection.", messageType: messageType)
/*        DispatchQueue.main.async {
            CNShared.getWindow().addNotification(color: messageType, message: "Please check your internet connection.")
        }
 */
    }
    
    class func showMessage(message: String, viewController: UIViewController, actions:[UIAlertAction]) {
       DispatchQueue.main.async {
        let alertController = UIAlertController(title: message, message: "", preferredStyle: UIAlertController.Style.alert)
        for actionItem in actions {
             alertController.addAction(actionItem)
        }
        viewController.present(alertController, animated: true, completion: nil)
        }
    }
}
