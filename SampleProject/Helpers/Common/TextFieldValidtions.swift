//
//  TextFieldValidtions.swift
//  PlinkedUser
//
//  Created by APPLE on 21/05/21.
//

import UIKit

class TextFieldValidtions: NSObject {
    
    static let txtFldInstance = TextFieldValidtions()
    
    public func validateName(name: String) -> Bool {
        // Length be 18 characters max and 3 characters minimum, you can always modify.
        let nameRegex = "^\\w{3,18}$"
        let trimmedString = name.trimmingCharacters(in: .whitespaces)
        let validateName = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        let isValidateName = validateName.evaluate(with: trimmedString)
        return isValidateName
    }
    
    public func validateName(string: String, txtFld: UITextField, range: NSRange) -> Bool {
        
        if range.location == 0 && string == " " { // prevent space on first character
            return false
        }
        
        if txtFld.text?.last == " " && string == " " { // allowed only single space
            return false
        }
        if string == " " { return true } // now allowing space between name
        
        if string.rangeOfCharacter(from: CharacterSet.letters.inverted) != nil {
            return false
        }
        let currentText = txtFld.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return true && updatedText.count < 25
        
    }
    
    public func validateEmailIdName(string: String, txtFld: UITextField, range: NSRange) ->Bool {
        
        if range.location == 0 && string == " " { // prevent space on first character
            return false
        }
        if string == "@" || string == "." { // allowed only single space
            return true
        }
        // if string == " " { return true }
        if string.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil {
            return false
        }
        return true
    }
    
    public func validaPhoneNumber(phoneNumber: String) -> Bool {
        let phoneNumberRegex = "^[6-9]\\d{9}$"
        let trimmedString = phoneNumber.trimmingCharacters(in: .whitespaces)
        let validatePhone = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = validatePhone.evaluate(with: trimmedString)
        return isValidPhone
    }
    
    public func validateMobileNumber(string: String, txtFld: UITextField, range: NSRange) -> Bool {
        
        let phoneNumberSet = CharacterSet(charactersIn: "+0123456789")
        if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
            return false
        }
        let currentText = txtFld.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.count <= 10
    }
    
    public func validateOTPVerification(string: String, txtFld: UITextField, range: NSRange) -> Bool {
        
        let phoneNumberSet = CharacterSet(charactersIn: "+0123456789")
        if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
            return false
        }
        let currentText = txtFld.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.count <= 1
        
    }
    
    public func validateEmailId(emailID: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailID)
    }
    
    public func validatePassword(password: String) -> Bool {        
        let passRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{9,}$"
        let trimmedString = password.trimmingCharacters(in: .whitespaces)
        let validatePassord = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isvalidatePass = validatePassord.evaluate(with: trimmedString)
        return isvalidatePass
    }
    public func validateAnyOtherTextField(otherField: String) -> Bool {
        let otherRegexString = "Your regex String"
        let trimmedString = otherField.trimmingCharacters(in: .whitespaces)
        let validateOtherString = NSPredicate(format: "SELF MATCHES %@", otherRegexString)
        let isValidateOtherString = validateOtherString.evaluate(with: trimmedString)
        return isValidateOtherString
    }
    
}
