//
//  DisplayBanner.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 23/01/22.
//

import NotificationBannerSwift

class DisplayBanner {
    static let shared = DisplayBanner()
    
    class func successBanner(message: String?) {
        NotificationBannerQueue.default.removeAll()
        let banner = GrowingNotificationBanner(title: "Success", subtitle: message ?? "", leftView: UIImageView(image: UIImage(named: "icon-success")), style: .success)
        banner.duration = 0.8
        banner.show()
    }
    
    class func failureBanner(message: String?) {
        NotificationBannerQueue.default.removeAll()
        let banner = GrowingNotificationBanner(title: "Failure", subtitle: message ?? "", leftView: UIImageView(image: UIImage(named: "icon-error")), style: .warning)
        banner.duration = 0.8
        banner.show()
    }
    
    class func infoBanner(message: String?) {
        NotificationBannerQueue.default.removeAll()
        let banner = GrowingNotificationBanner(title: "Info", subtitle: message ?? "", style: .info)
        banner.duration = 1.0
        banner.show()
    }
}
