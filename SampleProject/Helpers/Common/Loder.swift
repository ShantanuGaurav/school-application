//
//  Loder.swift
//  SampleProject
//
//  Created by Shantanu Gaurav on 08/02/22.
//

class Loader {

    class func show(isInteractive: Bool = false) {
//        SKActivityIndicator.spinnerStyle(.spinningCircle)
//        SKActivityIndicator.spinnerColor(Color.appColor)
//        SKActivityIndicator.show("", userInteractionStatus: isInteractive)
        CommonUtilities.shared.showProgressBar()

    }
    class func hide() {
       // SKActivityIndicator.dismiss()
        CommonUtilities.shared.dismissProgressBar()

    }
 
}
