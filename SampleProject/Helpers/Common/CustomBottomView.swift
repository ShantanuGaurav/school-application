//
//  CustomBottomView.swift
//  PlinkedUser
//
//  Created by Vijay's Macbook on 23/07/21.
//

import UIKit

class CustomBottomView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        roundCorners([.topLeft, .topRight], radius: 20)
    }
}

class CustomLowerView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        roundCorners([.bottomLeft, .bottomRight], radius: 20)
    }
}



