//
//  AppRouter.swift
//  PlinkedUser
//
//  Created by Vijay's Macbook on 04/06/21.
//

import UIKit
import KRProgressHUD

enum AppRouter {
    
    // MARK: - Set as Window Root
    static func setAsWindowRoot(_ navigationController: UINavigationController) {
        kAppDelegate.window?.rootViewController = navigationController
        kAppDelegate.window?.becomeKey()
        kAppDelegate.window?.makeKeyAndVisible()
    }
    
    // MARK: - Open Settings
    static func openSettings(_ vc: UIViewController) {
        vc.showAlertWithOneAction(title: "Permission Alert".localized(), message: "Please change your privacy setting from the Settings app and allow access to location for your app".localized(), firstAction: "Settings".localized(), preferredStyle: .actionSheet, completionHandler: { (status) in
            if status {
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        })
    }
    
    // MARK: - Go to View Controller
    static func goToVC(viewController: UIViewController) {
        let nvc = UINavigationController(rootViewController: viewController)
        nvc.isNavigationBarHidden = true
        UIView.transition(with: kAppDelegate.window!, duration: 0.33, options: UIView.AnimationOptions.transitionCurlUp, animations: {
            kAppDelegate.window?.rootViewController = nvc
        }, completion: nil)
        kAppDelegate.window?.becomeKey()
        kAppDelegate.window?.makeKeyAndVisible()
    }
}

class CommonUtilities
{
    var window : UIWindow?
    static let shared = CommonUtilities()
    
    // MARK: - Set Root to Any View Controller
    func setRoot(_ vc:UIViewController){
        //MARK:- To create first time window object in Application
        if #available(iOS 13.0, *) {
            UIApplication.shared.windows.first?.rootViewController = vc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            let bounds = UIScreen.main.bounds
            self.window = UIWindow(frame: bounds)
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
    }
    
    // MARK: - Home View Controller
    func homeVC() {
        let vc = HomeVC.instantiate(fromAppStoryboard: .Main)
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.setNavigationBarHidden(true, animated: false)
        setRoot(navigationController)
    }
    
    // MARK: - Testing View Controller
    func testingVC() {
        let vc = LoginVC.instantiate(fromAppStoryboard: .Registration)
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.setNavigationBarHidden(true, animated: false)
        setRoot(navigationController)
    }
    
    // MARK: - Set Login View Controller
    func setLoginscreen() {
        let vc = LoginVC.instantiate(fromAppStoryboard: .Registration)
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.setNavigationBarHidden(true, animated: false)
        setRoot(navigationController)
    }
   
    // MARK: - Show ProgressBar
    func showProgressBar() {
        KRProgressHUD.show()
        KRProgressHUD.set(activityIndicatorViewColors: [Color.appColor])
    }
    
    // MARK: - Dismiss ProgressBar
    func dismissProgressBar() {
        KRProgressHUD.dismiss()
    }
    
    // MARK: - Logout Operations
    func logoutOperations(){
        //Clear UserDefaults
        resetDefaults()
    }
    
    // MARK: - Reset UserDefaults Value
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
    
    // MARK: Check to load screen
    func checkToLoadScreen(){
        let token =  kUserDefaults.string(forKey: AppKeys.token)
        if token == nil {
            CommonUtilities.shared.setLoginscreen()
        }else{
            CommonUtilities.shared.homeVC()
        }
    }
}

