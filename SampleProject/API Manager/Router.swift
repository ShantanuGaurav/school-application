//
//  Router.swift
//  Hyred
//
//  Created by Mobilecoderz1 on 24/05/21.
//

import Alamofire
import UIKit

public enum Router {
    public typealias Params = [String: Any]
    case subscriptionPurchase(params: Params)
    case subscriptionList
    case logout
    case getSavedList(params: Params)
    case getImagesList(params: Params)
    case login(params: Params)
    case register(params: Params)
    case categorylist(params: Params)
    case subFolderList(params: Params)
    case saveCollection(params: Params)
    case changePassword(params: Params)
    case forgotPassword(params: Params)
    case deleteSavedList(params: Params)
    case updateCollection(params: Params)
    case categoryImagelist(params: Params)
    case deleteAccount(params: Params)

}

extension Router {
    var requestConfig: (endPoint: String, params: Params?, method: Alamofire.HTTPMethod) {
        switch self {
        case .subscriptionPurchase(params: let params):
            return ("auth/subscription_purchase", params, .post)
        case .subscriptionList:
            return ("getsubscriptionlist", nil, .get)
        case .logout:
            return ("auth/logout", nil, .post)
        case .login(params: let params):
            return ("auth/login", params, .post)
        case .register(let params):
            return ("auth/register", params, .post)
        case .forgotPassword(params: let params):
            return ("forgotpassword", params, .post)
        case .subFolderList(params: let params):
            return ("auth/subfolderlist", params, .get)
        case .changePassword(params: let params):
            return ("auth/changepassword", params, .post)
        case .categorylist(params: let params):
            return ("auth/categorylist", params, .get)
        case .categoryImagelist(params: let params):
            return ("auth/catimagelist", params, .get)
        case .saveCollection(params: let params):
            return ("auth/savecollection", params, .post)
        case .getSavedList(params: let params):
            return ("auth/getcollection", params, .get)
        case .getImagesList(params: let params):
            return ("auth/getcollection-images", params, .get)
        case .deleteSavedList(params: let params):
            return ("auth/deletecollection", params, .delete)
        case .updateCollection(params: let params):
            return ("auth/updatecollection", params, .post)
        case .deleteAccount(params: let params):
            return ("auth/delete-user", params, .post)

        }
    }
}

extension Router:  Alamofire.URLRequestConvertible {
    public func asURLRequest () throws -> URLRequest {
        let config = self.requestConfig
        var baseURL: URL {
            switch self {
            case .register:
                return URL(string: ServerURL.baseURL)!
            default:
                return URL(string: ServerURL.baseURL)!
            }
        }
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(config.endPoint))
        urlRequest.httpMethod = config.method.rawValue
        
        if let token =  kUserDefaults.value(forKey: AppKeys.token) as? String{
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        if let params = config.params, params.count > 0 {
            switch config.method {
            case .get, .delete:
                var urlComponents = URLComponents(url: urlRequest.url!, resolvingAgainstBaseURL: false)!
                urlComponents.queryItems = params.map { URLQueryItem(name: $0, value: "\($1)")}
                urlRequest.url = urlComponents.url!
                return urlRequest
            default:
                return try JSONEncoding.default.encode(urlRequest, with: params)
            }
        } else {
            return try JSONEncoding.default.encode(urlRequest)
        }
    }
    
}

struct ServerURL {
    static let imageBaseURL = "http://school.moogcenter.org/"
    static let baseURL = "http://school.moogcenter.org/api/"
}

