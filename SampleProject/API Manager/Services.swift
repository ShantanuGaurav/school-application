//
//  Services.swift
//  Hyred
//
//  Created by Mobilecoderz1 on 24/05/21.
//

import PromiseKit
var service: ApiInterface?

struct Console{
    static func log(_ message: Any?){
        print(message ?? "Nothing to log")
        
    }
}
protocol ApiInterface {
    func subscriptionPurchase<T: Decodable>(params: [String: Any]) -> Promise<T>
    func subscriptionList<T: Decodable>() -> Promise<T>
    func logout<T: Decodable>() -> Promise<T>
    func getSavedList<T: Decodable>(params: [String: Any]) -> Promise<T>
    func getImagesList<T: Decodable>(params: [String: Any]) -> Promise<T>
    func login<T: Decodable>(params: [String: Any]) -> Promise<T>
    func register<T: Decodable>(params: [String: Any]) -> Promise<T>
    func forgotPassword<T: Decodable>(params: [String: Any]) -> Promise<T>
    func catImageList<T: Decodable>(params: [String: Any]) -> Promise<T>
    func categorylist<T: Decodable>(params: [String: Any]) -> Promise<T>
    func subFolderList<T: Decodable>(params: [String: Any]) -> Promise<T>
    func saveCollection<T: Decodable>(params: [String: Any]) -> Promise<T>
    func changePassword<T: Decodable>(params: [String: Any]) -> Promise<T>
    func deleteSavedList<T: Decodable>(params: [String: Any]) -> Promise<T>
    func updateCollection<T: Decodable>(params: [String: Any]) -> Promise<T>
    func deleteAccount<T: Decodable>(params: [String: Any]) -> Promise<T>

}

final class Services: NSObject, ApiInterface {
   
    
    func getImagesList<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.getImagesList(params: params))
    }
    
    func subscriptionPurchase<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.subscriptionPurchase(params: params))
    }
    func subscriptionList<T>() -> Promise<T> where T : Decodable {
        client.requestWithParams(.subscriptionList)
    }
    func updateCollection<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.updateCollection(params: params))
    }
    func deleteSavedList<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.deleteSavedList(params: params))
    }
    func getSavedList<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.getSavedList(params: params))
    }
    func saveCollection<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.saveCollection(params: params))
    }
    func catImageList<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.categoryImagelist(params: params))
    }
    func categorylist<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.categorylist(params: params))
    }
    func subFolderList<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.subFolderList(params: params))
    }
    func logout<T>() -> Promise<T> where T : Decodable {
        client.requestWithParams(.logout)
    }
    func login<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.login(params: params))
    }
    func register<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.register(params: params))
    }
    func changePassword<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.changePassword(params: params))
    }
    func forgotPassword<T>(params: [String : Any]) -> Promise<T> where T : Decodable {
        client.requestWithParams(.forgotPassword(params: params))
    }
    func deleteAccount<T>(params: [String : Any]) -> PromiseKit.Promise<T> where T : Decodable {
        client.requestWithParams(.deleteAccount(params: params))

    }
    private let client = Client()
    
}
