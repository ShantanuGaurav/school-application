//
//  AppDelegate+Services.swift
//  SampleProject
//
//  Created by Shivam Garg on 08/03/22.
//

import Foundation
import PromiseKit

extension AppDelegate {
    // MARK: - Subscription List API
    func getSubscriptionList() {
        CommonUtilities.shared.showProgressBar()
        firstly{
            service!.subscriptionList()
        }.done({ (model: SubscriptionListModel) in
            self.handleResponseSubscriptionList(model: model)
        }).catch({ (error) in
            DisplayBanner.infoBanner(message: error.localizedDescription)
        }).finally {
            CommonUtilities.shared.dismissProgressBar()
        }
    }

    // MARK: - Handle Response of Saved List API
    func handleResponseSubscriptionList(model: SubscriptionListModel) {
        if model.status == 200  || model.status == 201 {
            self.subscriptionListObj = model
            IAPManager.shared.fetchAvailableProducts(showLoader: false)
        } else {
            DisplayBanner.failureBanner(message: model.message)
        }
        Console.log(model)
    }
    
}

// MARK: - SubscriptionListModel
struct SubscriptionListModel: Codable {
    let status: Int?
    let message: String?
    let data: [SubscriptionListData]?
}

// MARK: - Datum
struct SubscriptionListData: Codable {
    let id, subfolderID: Int?
    let subfolderName: String?
    let subscriptionID: String?
    let createdAt, updatedAt: String?
    let defaultfolderID: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case subfolderID = "subfolder_id"
        case subfolderName = "subfolder_name"
        case subscriptionID = "subscription_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case defaultfolderID = "defaultfolder_id"
    }
}
