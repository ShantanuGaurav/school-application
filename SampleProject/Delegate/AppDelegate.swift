//
//  AppDelegate.swift
//  SampleProject
//
//  Created by RAVI K GUPTA on 04/01/22.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var subscriptionListObj : SubscriptionListModel?
   // var purchasedIds = [String]()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        window?.rootViewController = UIStoryboard(name: "LaunchScreen", bundle: nil).instantiateInitialViewController()
            window?.makeKeyAndVisible()

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
                
                service = Services()
                self.getSubscriptionList()
                IQKeyboardManager.shared.enable = true
                CommonUtilities.shared.checkToLoadScreen()
                UIView.appearance().isExclusiveTouch = true
            }
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}


extension AppDelegate {
    
    // MARK: Save Profile Data
    var savedProfileData: LoginData?{
        get {
            return getUserData()
        }
        set(value) {
            saveUserData(data: value)
        }
    }
    
    // MARK: Get User Data
    func getUserData() -> LoginData?{
        if let userData = kUserDefaults.object(forKey: AppKeys.userData) as? Data {
            let decoder = JSONDecoder()
            //decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try? decoder.decode(LoginData.self, from: userData)
        }
        return nil
    }
    
    // MARK: Save User Data
    func saveUserData(data: LoginData?){

        if let token = data?.token{
            kUserDefaults.setValue(token, forKey: AppKeys.token)
            
        }
     
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(data) {
            kUserDefaults.set(encoded, forKey: AppKeys.userData)
        }
    }
    
    // MARK: Logout User
    func logoutUser() {
        kUserDefaults.removeObject(forKey: AppKeys.purchasedFolders)
        kUserDefaults.removeObject(forKey: AppKeys.token)
        kUserDefaults.removeObject(forKey: AppKeys.userData)
        CommonUtilities.shared.setLoginscreen()
        //self.switchToLoginScreen()
    }
    
    // MARK: - Switch to login screen
    private func switchToLoginScreen() {
        
        let navigation = CWindow?.rootViewController as? UINavigationController
        let controllers  = navigation?.viewControllers
        if controllers?.count == 1{
            if controllers?.first?.isKind(of: LoginVC.self) ?? false{
                return
            }
        }
        
        kUserDefaults.removeObject(forKey: AppKeys.token)
        let loginVC = LoginVC.instantiate(fromAppStoryboard: .Registration)
        let nav = SwipeableNavigationController(rootViewController: loginVC)
        nav.setNavigationBarHidden(true, animated: false)
        
    }
}
